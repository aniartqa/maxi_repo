﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TestFramework.General
{
    class Performance
    {
        public static IWebDriver driver { get => Drivers.dr; }
        

        public static void GetDataPerformance( string url , string comm)
        {
            Dictionary<string, object> collection1 = ComparePerformance(GetPerformance(url));
            WriteData(collection1);
        }

        public static Dictionary<string, object> GetPerformance(string url)
        {
            driver.Navigate().GoToUrl(url);

            object result1 = ((IJavaScriptExecutor)driver).ExecuteScript("var performance = window.performance || {};" +
            "var timings = performance.timing || {};" +
            "return timings;");

            return (Dictionary<string, object>)result1;
        }



        public static Dictionary<string, object> ComparePerformance(Dictionary<string, object> colections)
        {
            Dictionary<string, object> result1 = new Dictionary<string, object>();

            result1.Add("connectStart/connectEnd",
                UnixTimeToDateTime(Convert.ToInt64(colections["connectEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["connectStart"]))));
            result1.Add("domComplete",
                UnixTimeToDateTime(Convert.ToInt64(colections["domComplete"])));
            result1.Add("domContentLoadedEventEnd/domContentLoadedEventStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["domContentLoadedEventEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["domContentLoadedEventStart"]))));
            result1.Add("domInteractive",
                UnixTimeToDateTime(Convert.ToInt64(colections["domInteractive"])));
            result1.Add("domLoading",
                UnixTimeToDateTime(Convert.ToInt64(colections["domLoading"])));
            result1.Add("domainLookupEnd/domainLookupStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["domainLookupEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["domainLookupStart"]))));
            result1.Add("fetchStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["fetchStart"])));
            result1.Add("loadEventEnd/loadEventStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["loadEventEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["loadEventStart"]))));
            result1.Add("navigationStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["navigationStart"])));
            result1.Add("redirectEnd/redirectStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["redirectEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["redirectStart"]))));
            result1.Add("requestStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["requestStart"])));
            result1.Add("responseEnd/responseStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["responseEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["responseStart"]))));
            result1.Add("secureConnectionStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["secureConnectionStart"])));
            result1.Add("unloadEventEnd/unloadEventStart",
                UnixTimeToDateTime(Convert.ToInt64(colections["unloadEventEnd"])).Subtract(UnixTimeToDateTime(Convert.ToInt64(colections["unloadEventStart"]))));
            return result1;

        }

        public static DateTime UnixTimeToDateTime(long unixtime)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixtime).ToLocalTime();
            return dtDateTime;
        }

        public static void CompareDate(Dictionary<string, DateTime> collection1, Dictionary<string, DateTime> collection2)
        {
            for (int i = 0; i < collection1.Count; i++)
            {
                var item1 = collection1.ElementAt(i);
                var item2 = collection2.ElementAt(i);
                DateTime dtDateTime1 = item1.Value;
                DateTime dtDateTime2 = item2.Value;
                System.TimeSpan diff = dtDateTime1.Subtract(dtDateTime2);
                Console.WriteLine(item1.Key + ' ');
                Console.Write(diff.ToString());
            }
        }

        public static Dictionary<string, DateTime> ReadData(string file)
        {
            Dictionary<string, DateTime> result = File.ReadAllLines(file)
                                      .Select(x => x.Split('='))
                                      .ToDictionary(x => x[0], x => UnixTimeToDateTime(Convert.ToInt64(x[1])));
            return result;
        }


        public static void WriteData(Dictionary<string, object> collection1)
        {
            DateTime localDateF = DateTime.Now;
            string str = localDateF.ToString("MMddyyyyhhmmss");
            string numb = Regex.Replace(str, @"[:.\s]+", "");
            File.WriteAllLines(numb + "_date" + ".txt",
            collection1.Select(x => x.Key + "=" + x.Value).ToArray());

        }
    }


}
