﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TestFramework.Pages;

namespace TestFramework.General
{
    class Step // класс для обернутых в методы шагов(степов) теста
    {
        
        public static IWebDriver driver { get => Drivers.dr; }// Ссылка на обьект драйвера в классе Driver
        public static int Wtime { get => Drivers.Wt; }// Ссылка на переменную времени ожидания по умолчанию в классе Driver

        
        /// <summary>
        /// Нажимает на веб элемент
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <param name="comm">строка-коментарий</param>
        public static void Click(IWebElement element, string comm)
        {
            element.Click();
        }

        /// <summary>
        /// Пишет в веб элемент текст
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <param name="keys">текст</param>
        /// <param name="comm">строка-коментарий</param>
        public static void SendKeys(IWebElement element, string keys, string comm)
        {
            element.SendKeys(keys);
        }

        /// <summary>
        /// Переходит на страницу
        /// </summary>
        /// <param name="page">страница</param>
        /// <param name="comm">строка-коментарий</param>
        public static void Navigate(PageBase page, string comm)
        {
            page.Navigate();
        }

        /// <summary>
        /// Переключаеться на открытое окно соответствующего названия
        /// </summary>
        /// <param name="window">название окна</param>
        /// <param name="comm">строка-комментарий</param>
        public static void SwitchWindow(string window, string comm)
        {
            driver.SwitchTo().Window(window);
        }

        /// <summary>
        /// Ждет пока откроеться окно
        /// </summary>
        /// <param name="time">максимальное время ожидания</param>
        /// <param name="comm">строка-комментарий</param>
        public static void WaitOpenWindow(int time, string comm)
        {
            GeneralFunctions.WaitOpenWindow();

        }

        /// <summary>
        /// Наводит курсор на веб элемент, пролистывая его к верху экрана
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <param name="comm">строка-комментарий</param>
        public static void MoveTo(IWebElement element, string comm)
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);
        }



        /// <summary>
        /// Наводит курсор на веб элемент, пролистывая его к низу экрана
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <param name="comm">строка-комментарий</param>
        public static void Hover(IWebElement element, string comm)
        {
            var actions = new Actions(driver);
            actions.MoveToElement(element).Perform();
        }

        /// <summary>
        /// Выполняет ожидание по умолчанию в веб-элементе
        /// </summary>
        /// <param name="element">веб-элемент</param>
        /// <param name="comm">строка-комментарий</param>
        public static void Wait(IWebElement element, string comm)
        {
        }


        /// <summary>
        /// Выполняет ожидание по умолчанию в функции с ожиданием
        /// </summary>
        /// <param name="element">веб-элемент</param>
        /// <param name="comm">строка-комментарий</param>
        public static void Wait(bool element, string comm)
        {
        }

        /// <summary>
        /// Переходит на страницу выбраного через API запрос продукта
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void SomeProductPageOpen(string comm)
        {
            driver.Navigate().GoToUrl(Products.SomeProduct.ProductUrlOpen);
        }

        /// <summary>
        /// Переходит на страницу выбраного через API запрос второго продукта
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AnotherProductPageOpen(string comm)
        {
            driver.Navigate().GoToUrl(Products.AnotherProduct.ProductUrlOpen);
        }

        /// <summary>
        /// Авторизируеться через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void Authoryzation(string comm)
        {
            try
            {
                Apis.Authoryze().Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Добавляет выбранный продукт в корзину через API запрос 
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AddSomeProductCart(string comm)
        {
            try
            {
                Apis.AddProductToCart(Products.SomeProduct.ProductId).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Добавляет второй выбранный продукт в корзину через API запрос 
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AddAnotherProductCart(string comm)
        {
            try
            {
                Apis.AddProductToCart(Products.AnotherProduct.ProductId).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Добавляет выбранный продукт в лист желаний через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AddToWishList(string comm)
        {
            try
            {
                Apis.AddToWishlist(Products.SomeProduct.ProductId).Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Добавляет выбранный продукт в оформление покупки через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AddToCheckIn(string comm)
        {
            try
            {
                Apis.AddToCheckIn().Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Получает ключевую информацию о доступном на сайте продукте через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void GetSomeProduct(string comm)
        {
            try
            {
                Apis.SomeExistProduct("meiset-texnikasi/yemek-hazirlanmasi-ve-qizdirilmasi/mikrodalqali-soba/", "Sm", 0).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        /// <summary>
        /// Получает ключевую информацию о доступном на сайте втором продукте через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void GetAnotherProduct(string comm)
        {
            try
            {
                Apis.SomeExistProduct("televizorlar-ve-audio-video/televizorlar/", "An", 1).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        /// <summary>
        /// Удаляет все товары в листе желаний через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void DeleteAllProductsWishlist(string comm)
        {
            try
            {
                Apis.DeleteAllProductsWish().Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        /// <summary>
        /// Получает ключевую информацию о доступных на сайте двух товарах из группы Телевизоры через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void GetTwoProductsTV(string comm)
        {
            try
            {
                Apis.SomeExistProduct("televizorlar-ve-audio-video/televizorlar/", "Sm", 0).Wait();
                Apis.SomeExistProduct("televizorlar-ve-audio-video/televizorlar/", "An", 1).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        /// <summary>
        /// Получает ключевую информацию о доступном на сайте продукте из раздела мышек через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void GetSomeProductMouse(string comm)
        {
            try
            {
                Apis.SomeExistProduct("komputer-texnikasi/komputer-ucun-aksesuarlar/komputer-sicani/", "Sm", 0).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        /// <summary>
        /// Добавляет выбранный товар в сравнение через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AddSomeProductCompere(string comm)
        {
            try
            {
                Apis.AddToCompare(Products.SomeProduct.ProductId).Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Добавляет второй выбранный товар в сравнение через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void AddAnotherProductCompere(string comm)
        {
            try
            {
                Apis.AddToCompare(Products.AnotherProduct.ProductId).Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Удаляет все продукты в корзине через API запрос
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void DeleteAllProductsInCart(string comm)
        {
            try
            {
                Apis.DeleteAllProductsCart().Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }


        /// <summary>
        /// Открывает 6 страниц продуктов
        /// </summary>
        /// <param name="comm">строка-комментарий</param>
        public static void OpenProductPages(string comm)
        {
            try
            {
                Apis.OpenProductPages("telefonlar-ve-plansetler/telefonlar/", 6).Wait();
            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        /// <summary>
        /// Ждет пока в корзине измениться сумма
        /// </summary>
        /// <param name="pc">обьект страницы корзины</param>
        /// <param name="sum">изначальная сумма</param>
        /// <param name="comm">строка-комментарий</param>
        public static void WaitSumChange(PageCart pc, double sum, string comm)
        {
            PageCart.WaitSumChange(pc, sum);
        }

        /// <summary>
        /// Ждет пока текст в веб элементе будет отличаться от заданой строки, по истечению времени выбрасывает ошибку
        /// </summary>
        /// <param name="selector">css селектор веб элемента</param>
        /// <param name="text">строка</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <param name="comm">строка-комментарий</param>
        public static void WaitTextChange(By selector, string text, int time, string comm)
        {
            GeneralFunctions.WaitDifferentText(selector, text, time);
        }

        /// <summary>
        /// Очищает текстовое поле и вписывает в него строку
        /// </summary>
        /// <param name="field">веб элемент текстового поля</param>
        /// <param name="text">строка</param>
        /// <param name="comm">строка-комментарий</param>
        public static void CleanAndSendTextField(IWebElement field, string text, string comm)
        {
            GeneralFunctions.CleanAndSendTextTextField(field, text);
        }

        /// <summary>
        /// Переходит на указанную страницу
        /// </summary>
        /// <param name="path">URL-путь</param>
        /// <param name="comm">строка-комментарий</param>
        public static void NavigateOnPage(string path, string comm)
        {
            driver.Navigate().GoToUrl(path);
        }

        /// <summary>
        /// Комментарий к результату метода
        /// </summary>
        /// <param name="text">результат</param>
        /// <param name="comm">строка-комментарий</param>
        /// <returns>результат метода</returns>
        public static object GetValue(object obj, string comm)
        {
            return obj;
        }


        /// <summary>
        /// Проверяет что результат запроса не содержит сообщений об ошибках
        /// </summary>
        /// <param name="result">результат запроса</param>
        /// <param name="comm">строка-комментарий</param>
        /// <returns></returns>
        public static bool ApiError(string result, string comm)
        {
            MatchCollection Status = Regex.Matches(result, @"ERROR");
            return Status.Count == 0;
        }


        /// <summary>
        /// Проверяет что результат запроса содержит строку
        /// </summary>
        /// <param name="result">результат запроса</param>
        /// <param name="textSuccess">строка</param>
        /// <param name="comm">строка-комментарий</param>
        /// <returns></returns>
        public static bool ApiResultCorrect(string result, string textSuccess, string comm)
        {
            MatchCollection Status = Regex.Matches(result, textSuccess);
            return Status.Count > 0;
        }

        /// <summary>
        /// Проверяет что результат запроса не содержит строку
        /// </summary>
        /// <param name="result">результат запроса</param>
        /// <param name="textFall">строка</param>
        /// <param name="comm">строка-комментарий</param>
        /// <returns></returns>
        public static bool ApiFall(string result, string textFall, string comm)
        {
            MatchCollection Status = Regex.Matches(result, textFall);
            return Status.Count == 0;

        }

        /// <summary>
        /// Получает ключевую информацию о доступном на сайте продукте
        /// </summary>
        /// <param name="url">url страницы товаров</param>
        /// <param name="comm">строка-комментарий</param>
        public static void GetSomeProduct(string url,string comm)
        {
            try
            {
                Apis.SomeExistProduct(url, "Sm", 0).Wait();

            }
            catch (AggregateException ae)
            {
                Console.WriteLine($"EXCEPTION: {ae.Message}");
            }
        }

        public static void Navigate(string url, string comm)
        {
            driver.Navigate().GoToUrl(url);
        }

    }
}
