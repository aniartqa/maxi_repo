﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.General
{
    public class Product
    {
        public string ProductUrl;
        public string ProductId;
        public string ProductName;
        public string ProductNameSpaces;
        public double ProductPrice;
        public string ProductUrlOpen;

        public Product(string ProductUrl,string ProductId, string ProductName, double ProductPrice, string ProductUrlOpen) 
        {
            this.ProductUrl = ProductUrl;
            this.ProductId = ProductId;
            this.ProductName = ProductName;
            this.ProductPrice = ProductPrice;
            this.ProductUrlOpen = ProductUrlOpen;
        }
        public Product()
        {

        }
    }
}
