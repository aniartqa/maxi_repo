﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using TestFramework.Pages;
using TestFramework.Pages.Authorythation;

namespace TestFramework.General
{
    public delegate IWebElement Condition(IWebElement element);
    public delegate bool Check();



    class GeneralFunctions
    {
        public static IWebDriver driver { get => Drivers.dr; }// Ссылка на обьект драйвера в классе Driver
        public static int Wtime { get => Drivers.Wt; }// Ссылка на переменную времени ожидания по умолчанию в классе Driver



        /// <summary>
        /// Ищет на странице веб элемент по сss селектору
        /// </summary>
        /// <param name="driver">обьект драйвера</param>
        /// <param name="selector">css селектор веб элемента</param>
        /// <param name="timeSeconds">максимальное время ожидания</param>
        /// <param name="cnd">обьект делегата Condition</param>
        /// <returns>веб элемент</returns>
        public static IWebElement WaitElement(IWebDriver driver, By selector, int timeSeconds, Condition cnd)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeSeconds));
            return wait.Until((x) =>
            {
                IWebElement element = driver.FindElement(selector);

                return cnd(element);
            });
        }

        /// <summary>
        /// Оставляет в списке веб элементов только отображаемые на странице элементы
        /// </summary>
        /// <param name="elements">список веб элементов</param>
        /// <returns>список из отображаемых на странице элементов</returns>
        public static List<IWebElement> CheckElementsListDisplayed(List<IWebElement> elements)
        {
            List<IWebElement> elemsDisplayed = new List<IWebElement>();
            foreach (IWebElement element in elements)
            {
                if (element.Displayed) { elemsDisplayed.Add(element); }
            }
            return elemsDisplayed;
        }

        /// <summary>
        /// Проверяет что веб элемент существует, отображен на сайте и кликабельный
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <returns>при успехе - веб эемент, при провале - null</returns>
        public static IWebElement ExistDisplayedEnabled(IWebElement element)
        {
            return (element != null && element.Displayed && element.Enabled) ? element : null;
        }


        /// <summary>
        /// Проверяет что веб элемент существует и отображен на сайте
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <returns>при успехе - веб эемент, при провале - null</returns>
        public static IWebElement ExistDisplayed(IWebElement element)
        {
            return (element != null && element.Displayed) ? element : null;
        }

        /// <summary>
        /// Проверяет что веб элемент существует
        /// </summary>
        /// <param name="element">веб элемент</param>
        /// <returns>при успехе - веб эемент, при провале - null</returns>
        public static IWebElement Exist(IWebElement element)
        {
            return (element != null) ? element : null;
        }

        public static IWebElement NotDisplayed(IWebElement element)
        {
            return (!element.Displayed) ? element : null;
        }

        /// <summary>
        /// Удаляет в строке буквы и пробелы
        /// </summary>
        /// <param name="str">строка</param>
        /// <returns>строка без букв и пробелов</returns>
        public static string LeftNumbers(string str)
        {
            string num = Regex.Replace(str, @"[a-zA-Z\s]+", "");
            return num;
        }

        /// <summary>
        /// Удаляет в строке пробелы
        /// </summary>
        /// <param name="str">строка</param>
        /// <returns>строка без пробелов</returns>
        public static string NoSpaces(string str)
        {
            string nspace = Regex.Replace(str, @"[\s]+", "");
            return nspace;
        }

        /// <summary>
        /// Ждет пока текст в веб элементе будет равен заданой строке
        /// </summary>
        /// <param name="selector">css селектор веб элемента</param>
        /// <param name="text">строка</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <returns></returns>
        public static bool WaitSameText(By selector, string text, int time)
        {
            for (int i = 0; i < Wtime * 2; i++)
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(time)).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(selector));
                IWebElement element = driver.FindElement(selector);
                string elementText = NoSpaces(element.Text);
                if (elementText == text) { return true; }
                Thread.Sleep(500);
            }

            return false;
        }

        /// <summary>
        /// Ждет пока число в веб элементе будет равно заданому числу
        /// </summary>
        /// <param name="selector">css селектор веб элемента</param>
        /// <param name="num">число</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <returns></returns>
        public static bool WaitSameNumbers(By selector, double num, int time)
        {
            for (int i = 0; i < Wtime * 2; i++)
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(time)).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(selector));
                IWebElement element = driver.FindElement(selector);
                double elementNum = double.Parse(GeneralFunctions.LeftNumbers(element.Text), System.Globalization.CultureInfo.InvariantCulture);
                if (elementNum == num) { return true; }
                Thread.Sleep(500);
            }

            return false;
        }

        /// <summary>
        /// Ждет пока длинна списка измениться
        /// </summary>
        /// <param name="list">css селектор веб элемента</param>
        /// <param name="compereLength">первоначальная длинна списка</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <returns>максимальное время ожидания</returns>
        public static bool WaitDifferentLists(By list, int compereLength, int time)
        {
            if (compereLength == 0) { return false; }
            List<IWebElement> listFirst;

            for (int i = 0; i < Wtime * 2; i++)
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(time)).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(list));
                listFirst = driver.FindElements(list).ToList();
                listFirst = CheckElementsListDisplayed(listFirst);

                int a = listFirst.Count();
                if (a != compereLength && a > 0) { return true; }
                Thread.Sleep(500);
            }
            return false;
        }

        /// <summary>
        /// Ждет пока текст в веб элементе будет отличаться от заданой строки, по истечению времени выбрасывает ошибку
        /// </summary>
        /// <param name="selector">css селектор веб элемента</param>
        /// <param name="text">строка</param>
        /// <param name="time">максимальное время ожидания</param>
        public static void WaitDifferentText(By selector, string text, int time)
        {
            bool check = true;

            for (int i = 0; i < Wtime * 2; i++)
            {
                new WebDriverWait(driver, TimeSpan.FromSeconds(time)).Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(selector));
                IWebElement element = driver.FindElement(selector);
                string elementText = element.Text;
                if (elementText != text) { check = false; break; }
                Thread.Sleep(500);
            }
            if (check) { throw new ArgumentException("text is same"); }
        }


        /// <summary>
        /// Очищает текстовое поле и вписывает в него строку
        /// </summary>
        /// <param name="field">веб элемент текстового поля</param>
        /// <param name="text">строка</param>
        public static void CleanAndSendTextTextField(IWebElement field, string text)
        {
            Actions actions = new Actions(driver);

            actions.Click(field).KeyDown(Keys.Control).SendKeys("a").KeyUp(Keys.Control).SendKeys(Keys.Delete).SendKeys(text).SendKeys(Keys.Enter).Build().Perform();

        }

        /// <summary>
        /// Ждет пока веб элемент перестанет существовать на странице 
        /// </summary>
        /// <param name="selector">css селектор веб элемента</param>
        /// <returns></returns>
        public static bool WaitElementNotExist(By selector)
        {

            for (int i = 0; i < Wtime * 3; i++)
            {
                List<IWebElement> notExist = driver.FindElements(selector).ToList();
                if (!notExist.Any() && i > Wtime) { return true; }

                Thread.Sleep(500);
            }

            return false;
        }

        /// <summary>
        /// Ждет пока откроется новое окно
        /// </summary>
        public static void WaitOpenWindow()
        {
            bool check = true;
            for (int i = 0; i < Wtime * 3; i++)
            {

                IList<string> totWindowHandles = new List<string>(driver.WindowHandles);
                if (totWindowHandles.Count() >= 2) { check = false; break; }

                Thread.Sleep(500);
            }
            if (check) { throw new ArgumentException("window not open"); }

        }
    }
}
