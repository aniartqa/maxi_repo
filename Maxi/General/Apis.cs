﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using TestFramework.Pages.Authorythation;
using System.IO;
using System.Text;

namespace TestFramework.General
{
    public delegate void Api();
    class Apis
    {
        public static IWebDriver driver { get => Drivers.dr; } // Ссылка на обьект драйвера в классе Driver

        public static string ResultAuthorization; //результат выполнения API запроса Авторизации
        public static string ResultAddProductWishlist;//результат выполнения API запроса на добавление товара в лист желаний
        public static string ResultDeleteAllProductsWishlist;//результат выполнения API запроса на удаление всех товаров в листе желаний
        public static string ResultAddProductToCart;//результат выполнения API запроса на добавление товара в корзину
        public static string ResultDeleteAllProductsCart;//результат выполнения API запроса на удаление всех товаров в корзине
        public static string ResultAddToCompare;//результат выполнения API запроса на добавление товара в сравнение
        public static string ResultAddToCheckIn;//результат выполнения API запроса на добавление товара в оформление покупки
        public static string ResultSomeExistProduct;//результат выполнения API запроса на получение ключевой информации о доступном на сайте товаре


        /// <summary>
        /// Выполняет API запрос на добавление товара в корзину
        /// </summary>
        /// <param name="productId">id товара (товар выбран выполнением API запроса SomeProduct() )</param>
        /// <returns></returns>
        public static async Task AddProductToCart(string productId)
        {
            driver.Navigate().GoToUrl(Drivers.baseUrlLocal);

            using var client = new HttpClient(GetCockie());

            var values = new Dictionary<string, string>
            {
                { "handler", "basket" },
                { "func", "add" },
                { "product-id", productId},
                { "lang", "ru"}
            };

            var content = new FormUrlEncodedContent(values);

            var result = await client.PostAsync(Drivers.baseUrl + "ajax/common.php", content);

            ResultAddProductToCart = await result.Content.ReadAsStringAsync();


            driver.Navigate().Refresh();
        }


        /// <summary>
        /// Выполняет API запрос на удаление всех товаров в корзине
        /// </summary>
        /// <returns></returns>
        public static async Task DeleteAllProductsCart()
        {
            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + "cart/");

            
            using var cl = new HttpClient(GetCockie());


            var resultProductsCartId = await cl.GetAsync(Drivers.baseUrlLocal + "cart/");
            var contentsProductsCartId = await resultProductsCartId.Content.ReadAsStringAsync();


            List<String> productsCartId = new List<String>();
            MatchCollection MatchesProductsCartId = Regex.Matches(contentsProductsCartId, @"cell-delete cdelete_hidden jsDelProduct"" data-id=""(.+?)""");

            foreach (Match match in MatchesProductsCartId)
            {
                string temp1 = Regex.Match(match.Value, @"data-id=""(.+?)""").Value;
                string temp2 = Regex.Match(temp1, @"""(.+?)""").Value;
                string productCartId = Regex.Match(temp2, @"(\w+)").Value;

                productsCartId.Add(productCartId);
            }

            if (productsCartId.Count == 0) { }

            foreach (string productCartId in productsCartId) {

                using var clientDeleteProductCart = new HttpClient(GetCockie());

                var values = new Dictionary<string, string>
                {
                    { "data", "updateBasketProduct" },
                    { "id", productCartId },
                    { "quantity", "0"},
                    { "lang", "ru"}
                };

                var content = new FormUrlEncodedContent(values);

                var result = await clientDeleteProductCart.PostAsync(Drivers.baseUrl + "ajax.php", content);

                ResultDeleteAllProductsCart = await result.Content.ReadAsStringAsync();

                driver.Navigate().Refresh();
            }

        }

        /// <summary>
        /// Выполняет API запрос на авторизацию
        /// </summary>
        /// <returns></returns>
        public static async Task Authoryze()
        {

            driver.Navigate().GoToUrl(Drivers.baseUrlLocal);
            using var cl = new HttpClient();

            var res = await cl.GetAsync(Drivers.baseUrlLocal);
            var contents = await res.Content.ReadAsStringAsync();

            string resultString = Regex.Match(contents, @"name=""csrf-token"" content=""(.+?)""").Value;
            string res1 = Regex.Match(resultString, @"content=""(.+?)""").Value;
            string res2 = Regex.Match(res1, @"""(.+?)""").Value;
            string res3 = Regex.Match(res2, @"(\w+)").Value;


            driver.Navigate().GoToUrl(Drivers.baseUrlLocal);
            var cook = driver.Manage().Cookies.AllCookies;

            
            using var client = new HttpClient(GetCockie());


            var values = new Dictionary<string, string>
            {
                { "backurl", "/ajax.php" },
                { "AJAX", "Y" },
                { "data", "auth"},
                { "template", "" },
                { "sessid", res3 },
                { "lang", "ru"},
                { "AUTH_FORM", "Y" },
                { "TYPE", "AUTH" },
                { "USER_LOGIN", Users.UserAli.Email},
                { "USER_PASSWORD", Users.UserAli.Password},
                { "USER_REMEMBER", "N" },
                { "undefined", "Отмена"},
                { "redirect", "/"}
            };

            var content = new FormUrlEncodedContent(values);

            var result = await client.PostAsync(@Drivers.baseUrl + "ajax.php", content);


            ResultAuthorization = await result.Content.ReadAsStringAsync();

            driver.Navigate().Refresh();

        }


        /// <summary>
        /// Выполняет API запрос на добавление товара в лист желаний
        /// </summary>
        /// <param name="productId">id товара (товар выбран выполнением API запроса SomeProduct() )</param>
        /// <returns></returns>
        public static async Task AddToWishlist(string productId)
        {

            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + "eylence-ve-istirahet/oyun-zonasi/");
            
            using var client = new HttpClient(GetCockie());

            var res = await client.GetAsync(Drivers.baseUrlLocal + "eylence-ve-istirahet/oyun-zonasi/");
            var contents = await res.Content.ReadAsStringAsync();

            string resultString = Regex.Match(contents, @"name=""csrf-token"" content=""(.+?)""").Value;
            string res1 = Regex.Match(resultString, @"content=""(.+?)""").Value;
            string res2 = Regex.Match(res1, @"""(.+?)""").Value;
            string sessionId = Regex.Match(res2, @"(\w+)").Value;


            var addToWishlistValues = new Dictionary<string, string>
            {
                { "submit", "1" },
                { "ACTION", "add_item"},
                { "PRODUCT_ID", productId },
                { "FORM_ACTION", "/ajax/common.php?sessid="+sessionId},
                { "bxajaxidjQuery", "" },
                { "AJAX_CALLjQuery", "Y" },
                { "GROUP_ID", "1570"}, //1440 // 1570 прод //1442
                { "NAME", "" },
                { "handler", "Favorites" },
                { "func", "process"},
                { "lang", "ru" }
            };
            using var client2 = new HttpClient(GetCockie());

            var addToWishlistContent = new FormUrlEncodedContent(addToWishlistValues);


            var addToWishlistResult = await client2.PostAsync(Drivers.baseUrl + "ajax/common.php?sessid=" + sessionId, addToWishlistContent);

            ResultAddProductWishlist = await addToWishlistResult.Content.ReadAsStringAsync();

            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + "personal/wishlist/");

        }

        /// <summary>
        /// Выполняет API запрос на удаление всех товаров в листе желаний
        /// </summary>
        /// <returns></returns>
        public static async Task DeleteAllProductsWish()
        {
            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + "personal/wishlist/");

            using var cl = new HttpClient(GetCockie());


            var resultProductsWishId = await cl.GetAsync(Drivers.baseUrlLocal + "personal/wishlist/");
            var contentsProductsWishId = await resultProductsWishId.Content.ReadAsStringAsync();

            string resultString = Regex.Match(contentsProductsWishId, @"name=""csrf-token"" content=""(.+?)""").Value;
            string res1 = Regex.Match(resultString, @"content=""(.+?)""").Value;
            string res2 = Regex.Match(res1, @"""(.+?)""").Value;
            string sessionId = Regex.Match(res2, @"(\w+)").Value;

            List<String> productsWishId = new List<String>();
            MatchCollection MatchesProductsWishId = Regex.Matches(contentsProductsWishId, @"ACTION=delete_item&amp;ITEM_ID=(.+?)&amp");

            for (int i = 0; i < MatchesProductsWishId.Count; i++)
            {
                string productWishId = Regex.Match(MatchesProductsWishId[i].Value, @"(\d+)").Value;
                if (i == 0) { productsWishId.Add(productWishId); }
                else
                {
                    bool check = true;
                    foreach (string id in productsWishId)
                    {
                        if (id == productWishId) { check = false; break; }
                    }
                    if (check) { productsWishId.Add(productWishId); }
                }
            }

            foreach (string productWishId in productsWishId)
            {

                using var clientDeleteProductWish = new HttpClient(GetCockie());

                var values = new Dictionary<string, string>
                {
                    { "ACTION", "delete_item" },
                    { "ITEM_ID", productWishId },
                    { "SHOW_GROUP_ID", "1570"}, //1570 //1440 //1442
                    { "sessid", sessionId},
                    { "bxajaxid", "caeed5110aad0887516ad37c2b340910"}
                };

                var content = new FormUrlEncodedContent(values);

                var result = await clientDeleteProductWish.PostAsync(Drivers.baseUrlLocal +
                    "personal/wishlist/?ACTION=delete_item&ITEM_ID=" + productWishId + "&SHOW_GROUP_ID=1570&sessid=" + sessionId + "&bxajaxid=caeed5110aad0887516ad37c2b340910", content);

                ResultDeleteAllProductsWishlist = await result.Content.ReadAsStringAsync();


                driver.Navigate().Refresh();

            }
        }

        /// <summary>
        /// Выполняет API запрос на добавление товара в сравнение
        /// </summary>
        /// <param name="productId">id товара (товар выбран выполнением API запроса SomeProduct() )</param>
        /// <returns></returns>
        public static async Task AddToCompare(string productId)
        {
            driver.Navigate().GoToUrl(Drivers.baseUrlLocal);

            using var client = new HttpClient(GetCockie());


            var addToCompareValues = new Dictionary<string, string>
            {
                { "handler", "compare" },
                { "func", "addProductToCompare"},
                { "productId", productId },
                { "lang", "ru" }
            };

            var addToCompareContent = new FormUrlEncodedContent(addToCompareValues);

            var result = await client.PostAsync(Drivers.baseUrl + "ajax/common.php", addToCompareContent);


            ResultAddToCompare = await result.Content.ReadAsStringAsync();

            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + "compare/");

        }


        /// <summary>
        /// Выполняет API запрос на добавление всех товаров в корзине в оформление покупки
        /// </summary>
        /// <returns></returns>
        public static async Task AddToCheckIn()
        {

            driver.Navigate().GoToUrl("https://maxi.az/ru/");
            
            using var client = new HttpClient(GetCockie());

            var res = await client.GetAsync("https://maxi.az/ru/cart/");
            var contents0 = await res.Content.ReadAsStringAsync();

            string resultString0 = Regex.Match(contents0, @"cell-delete cdelete_hidden jsDelProduct"" data-id=""(.+?)""").Value;

            string res01 = Regex.Match(resultString0, @"data-id=""(.+?)""").Value;
            string res02 = Regex.Match(res01, @"""(.+?)""").Value;
            string res03 = Regex.Match(res02, @"(\w+)").Value;

            var addToCheckInValues1 = new Dictionary<string, string>
            {
                { "QUANTITY_INPUT_"+ res03, "1" },
                { "QUANTITY_" + res03, "1" },
                { "BasketOrder", "BasketOrder"}
            };

            var addToCheckInContent1 = new FormUrlEncodedContent(addToCheckInValues1);

            var result = await client.PostAsync(@"https://maxi.az/ru/cart/", addToCheckInContent1);

            ResultAddToCheckIn = await result.Content.ReadAsStringAsync();

            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + "order/");
        }

        /// <summary>
        /// Выполняет API запрос на получение ключевой информации о доступном на сайте товаре
        /// </summary>
        /// <param name="urlProductGroop">URL страницы с группой товаров</param>
        /// <param name="SomeOrAnother">"Sm"- запись полученных данных в хранилище первого продукта (Product SomeProduct),"An" - запись в хранилище второго продукта(Product AnotherProduct)</param>
        /// <param name="numElem">порядковый номер товара выбранного для получения данных в списке продуктов на странице</param>
        /// <returns></returns>
        public static async Task SomeExistProduct(string urlProductGroop, string SomeOrAnother, int numElem)
        {
            Product product = new Product();
            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + urlProductGroop);

            using var client = new HttpClient(GetCockie());

            var res = await client.GetAsync(Drivers.baseUrlLocal + urlProductGroop);
            var contents = await res.Content.ReadAsStringAsync();
            MatchCollection matches = Regex.Matches(contents, @"one-cat-item-i(.+?)<div id=""bx", RegexOptions.Singleline);

            string prUrl1 = Regex.Match(matches[numElem].Value, @"""one-cat-item-tit(.+?)a>", RegexOptions.Singleline).Value;
            string prUrl2 = Regex.Match(prUrl1, @"href=""(.+?)""").Value;
            string prUrl3 = Regex.Match(prUrl2, @"""(.+?)""").Value;
            string prUrl4 = Regex.Replace(prUrl3, @"[""]+", "");
            string productUrl = prUrl4.Remove(0, 4);

            product.ProductUrl = productUrl;
            product.ProductUrlOpen = Drivers.baseUrlLocal + productUrl;

            string prId1 = Regex.Match(matches[numElem].Value, @"data-id=""(.+?)""").Value;
            string prId2 = Regex.Match(prId1, @"""(.+?)""").Value;
            string productId = Regex.Match(prId2, @"(\w+)").Value;

            product.ProductId = productId;

            string prName1 = Regex.Match(prUrl1, @"class=""(.+?)<").Value;
            string prName2 = Regex.Match(prName1, @">(.+?)<").Value;
            string productName = Regex.Replace(prName2, @"[><]+", "");
            string productNameNoSpaces = Regex.Replace(productName, @"[\s]+", "");

            product.ProductName = productNameNoSpaces;
            product.ProductNameSpaces = productName;

            string prPrice1 = Regex.Match(matches[numElem].Value, @"<span class=""nowrap"">(.+?)<", RegexOptions.Singleline).Value;
            string prPrice2 = Regex.Match(prPrice1, @">(.+?)<", RegexOptions.Singleline).Value;
            string productPrice = Regex.Replace(prPrice2, @"[><\s]+", "");

            product.ProductPrice = double.Parse(productPrice, System.Globalization.CultureInfo.InvariantCulture);

            if (SomeOrAnother == "An")
            {
                Products.AnotherProduct = product;
            }
            else
            {
                Products.SomeProduct = product;
            }

           ResultSomeExistProduct = await res.Content.ReadAsStringAsync();

        }

        /// <summary>
        /// Открывает указанное количество страниц товаров
        /// </summary>
        /// <param name="urlProductGroop">URL страницы с группой товаров</param>
        /// <param name="count">количество страниц</param>
        /// <returns></returns>
        public static async Task OpenProductPages(string urlProductGroop, int count)
        {

            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + urlProductGroop);
            using var client = new HttpClient(GetCockie());

            var res = await client.GetAsync(Drivers.baseUrlLocal + urlProductGroop);
            var contents = await res.Content.ReadAsStringAsync();

            MatchCollection matches = Regex.Matches(contents, @"""one-cat-item-tit(.+?)a>", RegexOptions.Singleline);
            for (int i = 1; i <= count; i++)
            {
                string prUrl2 = Regex.Match(matches[i].Value, @"href=""(.+?)""").Value;
                string prUrl3 = Regex.Match(prUrl2, @"""(.+?)""").Value;
                string prUrl4 = Regex.Replace(prUrl3, @"[""]+", "");
                string productUrl = prUrl4.Remove(0, 4);

                driver.Navigate().GoToUrl(Drivers.baseUrlLocal + productUrl);
            }

        }


        /// <summary>
        /// Возвращает куки страницы в драйвере
        /// </summary>
        /// <returns>куки контейнер </returns>
        public static HttpClientHandler GetCockie()
        {
            var cook = driver.Manage().Cookies.AllCookies;
            CookieContainer cookies = new CookieContainer();

            foreach (var c in cook)
            {
                cookies.Add(new System.Net.Cookie(c.Name, c.Value, c.Path, c.Domain));
            }

            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;
            
            return handler;

        }


        /// <summary>
        /// Находит короткое имя для товара SomeProduct
        /// </summary>
        /// <returns>строка короткого имени</returns>
        public static string GetShortProductName() {

            string[] prodNames = (Products.SomeProduct.ProductNameSpaces).Split(" ");
            string prodName = "";

            foreach (string name in prodNames)
            {
                if (name.Length > 2) { return prodName = name; }
            }

            return prodName;
        }

    }
    
}
