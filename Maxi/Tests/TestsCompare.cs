﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;
using TestFramework.Pages.Authorythation;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Сравнение")]
    class TestsCompare<TBrowser> : TestBase<TBrowser> //Test Suite для проверки сравнения товаров
    {
        [Test]
        [Description("Выведено правильное название товара в сравнении")]
        public void ProductNameCompare()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");

            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.Hover(PMain.ImageCompare, "Навели курсор на иконку сравнения");
            Step.Click(PMain.ButtonCompare, "Нажали на кнопку перехода к сравнению");
            Step.Wait(PComp.TextCardProduct, "Дождались открытия страницы сравнения");

            Assert.AreEqual(Products.SomeProduct.ProductName, PComp.TextFirstProductName, 
                "Название товара в сравнении равно названию выбраного товара");
        }

        [Test]
        [Description("Выведена правильная цена товара в сравнении ")]
        public void ProductPriceCompare()
        {
            Step.GetTwoProductsTV("Выбрали два товара в одной группе");
            Step.AddSomeProductCompere("Добавили первый товар в сравнение");
            Step.AddAnotherProductCompere("Добавили второй товар в сравнение");

            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Assert.AreEqual(Products.AnotherProduct.ProductPrice, PComp.TextSecondProductPrice, 
                "Цена товара в сравнении равна цене выбранного товара");
        }


        [Test]
        [Description("Товар удален из сравнения")]
        public void DeleteProductFromCompare()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");

            Step.Navigate(PComp, "Перешли на страницу сравнения");
            Step.Hover(PComp.TextCardProduct, "Навели курсор на карточку товара");
            Step.Click(PComp.ButtonDeleteProduct, "Нажали на кнопку удаления товара в сравнении");

            Assert.IsTrue(PComp.TextEmptyProductList.Displayed, "Товар удален");
        }


        [Test]
        [Description("Выведены характеристики товара")]
        public void CharactersCompare()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");
            Step.SomeProductPageOpen("Перешли на страницу выбранного товара");
            string characterType = (string)Step.GetValue(PProd.TextCharacterType,
                "Запомнили значение первой характеристики товара");
            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Assert.AreEqual(characterType, PComp.TextCharacterType,
                "Характеристика выбранного товара равна характеристике товара в сравнении");
        }

        [Test]
        [Description("Показаны только различающиеся характеристики")]
        public void DiferentCharactersCompare()
        {
            Step.GetTwoProductsTV("Выбрали два товара в одной группе");
            Step.AddSomeProductCompere("Добавили первый товар в сравнение");
            Step.AddAnotherProductCompere("Добавили второй товар в сравнение");

            Step.Navigate(PComp, "Перешли на страницу сравнения");
            Step.Wait(PComp.ButtonGroop, "Дождались появления панели с кнопками на странице сравнения");
            PageCompare.ListCharacters = (List <IWebElement>)Step.GetValue( GeneralFunctions.CheckElementsListDisplayed(PComp.TextCharacters),
                "Запомнили первоначальный список характеристик");
            Step.Click(PComp.ButtonDiferentCharacters, "Нажали кнопку 'Показать только различия'");
            
            Assert.IsTrue(GeneralFunctions.WaitDifferentLists(PComp.TextCharactersSelector, 
                PageCompare.ListCharacters.Count(), 15), 
                "Текущий список характеристик содержит хотя бы одну характеристику и не равен первоначальному списку характеристик");
        }

        [Test]
        [Description("Товар из банера добавлен в сравнение")]
        public void BannerAddCompare()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.MoveTo(PMain.SliderLider, "Пролистали страницу до слайдера лидеров просмотра");
            Step.Hover(PMain.CardProductSliderLider, "Навели курсор на карточку товара");
            Step.Click(PMain.ButtonCompareSliderLider, "Нажали на кнопку добавить в сравнение");
            Step.Wait(PMain.ButtonCompareSliderLiderActive, 
                "Дождались активации кнопки добавления товара в сравнение");
            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Assert.IsTrue(PComp.ButtonGroop.Displayed, "Выбранный товар добавлен в сравнение");
        }


        [Test]
        [Description("Добавлен товар в сравнение из страницы сравнения")]
        public void AddProductCompare()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");

            Step.Navigate(PComp, "Перешли на страницу сравнения");
            Step.Click(PComp.ButtonAddProduct, "Нажали на кнопку дбавления товара в сравнение");

            Assert.IsTrue(PProdGroup.FormFilters.Displayed, "Открыта страница группы товаров");
        }


        [Test]
        [Description("Товар из группы товаров добавлен в сравнение")]
        public void PageGroupAddCompare()
        {
            Step.Navigate(PProdGroup, "Перешли на страницу группы");

            Step.Hover(PProdGroup.TextCardProduct, "Навели курсор на карточку товара");
            Step.Click(PProdGroup.ButtonCompare, "Нажали на кнопку добавить в сравнение");
            Step.Wait(PProdGroup.ButtonCompareActive, 
                "Дождались активации кнопки добавления товара в сравнение");
            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Assert.IsTrue(PComp.ButtonGroop.Displayed, "Выбранный товар добавлен в сравнение");
        }

        [Test]
        [Description("Товар из страницы товара добавлен в сравнение")]
        public void PageProductAddCompare()
        {
            Step.GetSomeProduct("Выбран товар");
            Step.SomeProductPageOpen("Открыта страница товара");
            Step.Click(PProd.ButtonCompare, "Нажали на кнопку добавить в сравнение");
            Step.Wait(PProd.ButtonCompareActive, 
                "Дождались активации кнопки добавления товара в сравнение");
            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Assert.AreEqual(Products.SomeProduct.ProductName, PComp.TextFirstProductName,
                "Название товара в сравнении равно названию выбраного товара");
        }

        [Test]
        [Description("Сравнение товаров распечатано")]
        public void PrintCompare()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");
            Step.GetAnotherProduct("Выбрали другой товар");
            Step.AddAnotherProductCompere("Добавили другой товар в сравнение");

            Step.Navigate(PComp, "Перешли на страницу сравнения");
            Step.Wait(PComp.ButtonGroop, "Дождались активации кнопки добавления товара в сравнение");
            Step.Click(PComp.ButtonsGroops[1], "Нажали на кнопку второй группы товаров");

            Step.Navigate(PComp, "Перешли на страницу сравнения");
            Step.Click(PComp.ButtonPrint, "Нажали кнопку печати");
            Step.WaitOpenWindow(Wtime, "Дождались открытия окна");
            Step.SwitchWindow(driver.WindowHandles.Last(), "Перешли на открывшуюся страницу");

            Assert.AreEqual(Products.AnotherProduct.ProductName,
                PComp.TextProductNamePrint, "Название выбранного товара выведено на странице распечатки");
        }


        [Test]
        [Description("Закрыта страница сравнения")]
        public void CloseCompare()
        {
            Step.Authoryzation("Авторизировались");
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");

            Step.Navigate(PComp, "Перешли на страницу сравнения");
            Step.Click(PComp.ButtonMainPage, "Нажали на кнопку возврата на главную страницу");

            Assert.IsTrue(PMain.SliderHome.Displayed, "Закрыта страница сравнения");
        }


        [Test]
        [Description("Все сравнения удалены")]
        public void DeleteAllCompares()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");

            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.Hover(PMain.ImageCompare, "Навели курсор на иконку сравнения");
            Step.Click(PMain.ButtonDeleteCompares, "Нажали на кнопку 'очистить все'");
            Step.Hover(PMain.ImageCompare, "Навели курсор на иконку сравнения");
            Step.Wait(PMain.TextEmptyCompare, "Дождались появления сообщениия о пустом листе сравнений");

            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Assert.IsTrue(PComp.TextEmptyProductList.Displayed, "Страница сравнения не содержит товаров");
        }

       
    }
}
