﻿using NUnit.Framework;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages;
using TestFramework.Tests;

namespace TestFramework
{

    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Корзина")]
    class TestsCart<TBrowser> : TestBase<TBrowser> //Test Suite для проверки корзины 
    {
        [Test]
        [Description("Товар из страницы товара добавлен в корзину")]
        public void CartAddPageProduct()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.SomeProductPageOpen("Перешли на страницу выбранного товара");

            Step.Click(PProd.ButtonBy, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");

        }

        [Test]
        [Description("Товар из кредита добавлен в корзину")]
        public void CartAddCredit()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.SomeProductPageOpen("Перешли на страницу выбранного товара");
            
            Step.Click(PProd.ButtonAllCredits, "Нажали на кнопку показать все кредиты");
            Step.Click(PProd.ButtonCredit, "Нажали на кнопку выбора кредита");
            Step.Wait(PProd.ButtonCreditActive, "Дождались активации кнопки выбора кредита");
            Step.Click(PProd.ButtonByCredit, "Нажали на кнопку оформления кредита");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оплаты");

            Assert.IsTrue(PCheck.ButtonMenuSteps.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Добавлено верное количество товара из кредита в козину")]
        //Тест покрывает переодически возникающий баг с добавлением нескольких товаров в корзину
        public void CartAddCreditProductCount()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.SomeProductPageOpen("Перешли на страницу выбранного товара");

            Step.Click(PProd.ButtonAllCredits, "Нажали на кнопку показать все кредиты");
            Step.Click(PProd.ButtonCredit, "Нажали на кнопку выбора кредита");
            Step.Wait(PProd.ButtonCreditActive, "Дождались активации кнопки выбора кредита");
            Step.Click(PProd.ButtonByCredit, "Нажали на кнопку оформления кредита");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оплаты");

            Assert.IsTrue(PCheck.Products.Count() >= 1 && PCheck.Products.Count() < 2, "Один товар добавлен в корзину");
        }

        [Test]
        [Description("Tовар из плавающего банера страницы товара добавлен в корзину")]
        public void CartAddPageProductBanner()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.SomeProductPageOpen("Перешли на страницу выбранного товара");

            Step.Hover(PProd.FormComments, "Пролистали страницу до слайдера просмотренных");
            Step.Click(PProd.ButtonByBanner, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из слайдера скидки добавлен в корзину")]
        public void CartAddSliderSale()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.MoveTo(PMain.SliderSale, "Пролистали страницу до слайдера скидки");
            Step.Hover(PMain.CardProductSliderSale, "Навели курсор на карточку товара");
            Step.Click(PMain.ButtonBySliderSale, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");
            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из слайдера лидеров просмотра добавлен в корзину")]
        public void CartAddSliderLider()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.MoveTo(PMain.SliderLider, "Пролистали страницу до слайдера лидеров просмотра");
            Step.Hover(PMain.CardProductSliderLider, "Навели курсор на карточку товара");
            Step.Click(PMain.ButtonBySliderLider, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из слайдера новинки добавлен в корзину")]
        public void CartAddSliderNew()
        {
            Step.Navigate(PMain,"Перешли на главную страницу");

            Step.MoveTo(PMain.SliderNew, "Пролистали страницу до слайдера новинки");
            Step.Hover(PMain.CardProductSliderNew, "Навели курсор на карточку товара");
            Step.Click(PMain.ButtonBySliderNew, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd,"Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }


        [Test]
        [Description("Товар из слайдера 'Вы просматривали' добавлен в корзину")]
        public void CartAddSliderSaw()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.OpenProductPages("Просмотрели 6 страниц товаров");
            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");

            Step.MoveTo(PProdGroup.SliderSaw, "Пролистали страницу до слайдера 'Вы просматривали'");
            Step.Hover(PProdGroup.CardProductSliderSaw, "Навели курсор на карточку товара");
            Step.Click(PProdGroup.ButtonBySliderSaw, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из банера 'Скидка дня' добавлен в корзину")]
        public void CartAddBannerSaleDay()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.Click(PMain.ButtonBySliderDaySell, "Нажали кнопку купить товар в банере 'Скидка дня'");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из сравнения товаров добавлен в корзину")]
        public void CartAddCompare()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");
            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Step.Hover(PComp.TextCardProduct, "Навели курсор на карточку товара");
            Step.Click(PComp.ButtonBy, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из плавающего банера сравнения товаров добавлен в корзину")]
        public void CartAddCompareBanner()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCompere("Добавили товар в сравнение");
            Step.Navigate(PComp, "Перешли на страницу сравнения");

            Step.Hover(PComp.Footer, "Пролистали страницу до футера");
            Step.Click(PComp.ButtonByBanner, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }

        [Test]
        [Description("Товар из группы товаров добавлен в корзину")]
        public void CartAddGroop()
        {
            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");

            Step.Hover(PProdGroup.TextCardProduct, "Навели курсор на карточку товара");
            Step.Click(PProdGroup.ButtonBy, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }


        [Test]
        [Description("Выведена правильная цена товара в корзине")]
        public void ProductPriceCart()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");
            List<double> saleInCart = PCart.NumbersSaleCart;

            Assert.AreEqual(Products.SomeProduct.ProductPrice - saleInCart[0],
                Double.Parse(GeneralFunctions.LeftNumbers(PCart.TextProductPrice.Text), 
                CultureInfo.InvariantCulture),"Цена товара в корзине равна цене выбранного товара"); 
        }

        [Test]
        [Description("Выведено правильное название товара в корзине")]
        public void ProductNameCart()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.AreEqual(Products.SomeProduct.ProductName,
                GeneralFunctions.NoSpaces(PCart.TextProductName.Text), "Название товара в корзине равно названию выбранного товара");
        }


        [Test]
        [Description("Выведена правильная сумма для двух товаров в корзине")]
        public void ProductsPriceCart()
        {
            Step.GetSomeProduct("Выбрали первый товар");
            Step.AddSomeProductCart("Добавили первый товар в корзину");
            Step.GetAnotherProduct("Выбран второй товар");
            Step.AddAnotherProductCart("Добавили второй товар в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");
            List<double> saleInCart = (List<double>)Step.GetValue(PCart.NumbersSaleCart,
                "Запомнили значение скидки в корзине если она есть");

            Assert.AreEqual(Products.SomeProduct.ProductPrice - saleInCart[0] + Products.AnotherProduct.ProductPrice - saleInCart[1], 
                PCart.NumberSum, "Сумма в корзине равна сумме цен первого и второго товаров с учетом скидки в корзине");
        }

        [Test]
        [Description("Выведена правильная сумма для нескольких единиц одного товара в корзине")]
        public void SameProductPriceCart()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");
            
            Step.Navigate(PCart, "Перешли на страницу корзины");
            double sumDuring = (double)Step.GetValue(PCart.NumberSum,
                "Запомнили значение суммы в корзине");
            Step.Click(PCart.ButtonCountPluss,"Увеличили количество товара на одину единицу");
            Step.WaitSumChange(PCart, sumDuring, "Дождались изменения значения суммы в корзине");
            List<double> saleInCart = (List<double>)Step.GetValue(PCart.NumbersSaleCart,
                "Запомнили значение скидки в корзине если она есть");

            Assert.AreEqual((Products.SomeProduct.ProductPrice - saleInCart[0]) * 2, PCart.NumberSum, 
                "Сумма в корзине равна сумме цен двух единиц выбранного товара с учетом скидки в корзине");
        }

        [Test]
        [Description("Все товары удалены из корзины")]
        public void DeleteProductCart()
        {
            Step.GetSomeProduct("Выбрали первый товар");
            Step.AddSomeProductCart("Добавили первый товар в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");
            Step.Click(PCart.ButtonDeleteFirstProduct, "Удалили продукт");
            Step.Wait(PCart.TextEmptyCart, "Дождались появления текста 'Ваша корзина пуста'");

            Assert.IsTrue(GeneralFunctions.WaitElementNotExist(PCart.ProductSelector), 
                "Товаров нет в корзине");
        }

        
        [Test]
        [Description("Выведена правильная сумма после удаления одного из нескольких товаров из корзины")]
        public void SumDeleteProductCart()
        {
            Step.GetSomeProduct("Выбрали первый товар");
            Step.AddSomeProductCart("Добавили первый товар в корзину");
            Step.GetAnotherProduct("Выбран второй товар");
            Step.AddAnotherProductCart("Добавили второй товар в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");
            Step.Wait(PCart.ButtonDeleteFirstProduct, "Дождались появления кнопки удаления товара в корзине");
            double sumDuring = (double)Step.GetValue(PCart.NumberSum,
                "Запомнили значение суммы в корзине");
            Step.Click(PCart.ButtonDeleteFirstProduct, "Удалили первый товар");
            Step.WaitSumChange(PCart, sumDuring, "Дождались изменения значения суммы в корзине");
            List<double> saleInCart = (List<double>)Step.GetValue(PCart.NumbersSaleCart,
                "Запомнили значение скидки в корзине если она есть");

            Assert.AreEqual(Products.AnotherProduct.ProductPrice - saleInCart[0], PCart.NumberSum, 
                "Сумма в корзине равна цене второго товара с учетом скидки в корзине");
        }

        [Test]
        [Description("Вернулись назад к каталогу товаров из корзины")]
        //Тест покрывает переодически возникающий баг с пропажей кнопки Вернуться назад в пустой корзине
        public void CloseCart()
        {
            Step.Navigate(PCart, "Перешли на страницу корзины");
            Step.Click(PCart.ButtonBack, "Нажали на кнопку Вернуться назад к покупкам");

            Assert.IsTrue(GeneralFunctions.WaitElementNotExist(PCart.TextBoxEmptyCartSelector), 
                "Вернулись назад к каталогу товаров из корзины");
        }


    }
}
