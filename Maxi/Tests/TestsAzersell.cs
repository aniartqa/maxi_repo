﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Номера Телефонов")]
    class TestsAzersell<TBrowser> : TestBase<TBrowser>//Test Suite для проверки группы товаров 'Номера телефонов' 
                                                      //Test Suite покрывает переодически возникающий баг с невыведенными номерами телефонов
    {
        [Test]
        [Description("Добавлен товар в козину из карточки товара Azercell")]
        public void AzercellByCardProduct()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");

            Step.Navigate(PAzcell, "Перешли на страницу Azercell");
            Step.Click(PAzcell.ButtonBuyCartProduct, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбраный товар добавлен в корзину");
        }

        [Test]
        [Description("Количество товаров Azercell больше 400 штук")]
        public void AzercellAmount()
        {
            Step.Navigate(PAzcell, "Перешли на страницу Azercell");

            Assert.IsTrue(int.Parse(GeneralFunctions.LeftNumbers(PAzcell.TextCountAzercell.Text)) >20,
                "Количество товаров Azercell больше 400 штук");
        }

        [Test]
        [Description("Добавлен товар в козину из страницы товара Azercell")]
        public void AzercellByPageProduct()
        {
            Step.Navigate(PAzcell, "Перешли на страницу Azercell");
            Step.Click(PAzcell.ButtonSecondPageAzercell, "Перешли на вторую страницу в пагинации");
            Step.Click(PAzcell.TextProductName, "Перешли на страницу номера телефона");
            Step.Click(PAzcell.ButtonBy, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбраный товар добавлен в корзину");
        }
    }
}
