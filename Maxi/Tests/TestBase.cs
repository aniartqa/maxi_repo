﻿using Maxi.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Pages;
using TestFramework.Pages.Authorythation;

namespace TestFramework.Tests
{
    /// <summary>
    /// Базовый класс для всех Test Suite классов
    /// </summary>
    /// <typeparam name="TBrowser">Тип браузера в котором запустятся тесты: Chrome, Firefox, string (запуск headless Chrome)</typeparam>
    
    public class TestBase<TBrowser> //Базовый класс для всех Test Suite классов
    {
        
        public IWebDriver driver { get => Drivers.dr; }// Ссылка на обьект драйвера в классе Driver
        public string baseUrl {get => Drivers.baseUrlLocal; }// Ссылка на URL главной страницы maxi в классе Driver
        public int Wtime { get => Drivers.Wt; }// Ссылка на переменную времени ожидания по умолчанию в классе Driver



        public PageMain PMain = new PageMain(); // обьект главной страницы

        public PageAuthotyzation PAuth = new PageAuthotyzation();// обьект страницы авторизации

        public PageRemindPassword PRemindPath = new PageRemindPassword();// обьект страницы восстановления пароля

        public PageRegistration PReg = new PageRegistration();// обьект страницы регистрации

        public PageFacebookAuth PFaceb = new PageFacebookAuth();// обьект страницы фейсбук

        public PageGoogleAuth PGgl = new PageGoogleAuth();// обьект страницы гугл

        public PageAzercell PAzcell = new PageAzercell();// обьект страницы номеров телефона

        public PageCart PCart = new PageCart();// обьект страницы корзины

        public PageProductGroup PProdGroup = new PageProductGroup();// обьект страницы группы товаров

        public PageCompare PComp = new PageCompare();// обьект страницы сравнения

        public PageProduct PProd = new PageProduct();// обьект страницы товара

        public PageWishlist PWish = new PageWishlist();// обьект страницы списка желаний

        public PageCheckout PCheck = new PageCheckout();// обьект страницы оформления покупки

        public PageSearch PSearch = new PageSearch();// обьект страницы поиска

        public PageCompletedOrders PComplOrd = new PageCompletedOrders();// обьект страницы поиска

        /// <summary>
        /// Создает обьект браузера, выполняеться перед каждым Test Suite-ом
        /// </summary>
        [SetUp]
        public void CreateDriver()
        {

            if (typeof(TBrowser) == typeof(ChromeDriver)) { new Drivers("ch"); }

            if (typeof(TBrowser) == typeof(FirefoxDriver)) { new Drivers("fr"); }

            if (typeof(TBrowser) == typeof(string)) { new Drivers("ch-hd"); }
        }

        /// <summary>
        /// Финализация обьекта браузера, удаление данных в програмной сущности товара, выполняеться после каждого Test Suite-а
        /// </summary>
        [TearDown]
        public void TeardownTest()
        {
            driver.Close();
            driver.Quit();

            Products.SomeProduct = null;
            Products.AnotherProduct = null;
        }
    }
}
