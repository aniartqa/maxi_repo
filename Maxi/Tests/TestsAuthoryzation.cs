using NUnit.Framework;
using TestFramework.Pages;
using TestFramework.Tests;
using System.Threading;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.Linq;
using System;
using TestFramework.General;
using OpenQA.Selenium;
using NUnit.Framework.Internal;
using TestFramework.Pages.Authorythation;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("�����������")]
    class TestsAutoryzation<TBrowser> : TestBase<TBrowser> //Test Suite ��� �������� �����������
    {

        [Test]
        [Description("������������� ������������������ ������������")]
        public void AvtoryzedUser()
        {
            Step.Navigate(PMain, "������� �� ������� ��������");

            Step.Click(PMain.ButtonUserEntrance, "������ ������ ����� � �������");
            Step.SendKeys(PAuth.TextBoxEmail, Users.UserAli.Email, "����� ������������������ ����� � ���� ������");
            Step.SendKeys(PAuth.TextBoxPassword, Users.UserAli.Password, "����� ������������������ ������ � ���� ������");

            Step.Click(PAuth.ButtonEntrance, "������ ������ ����� � ����� �����������");

            Assert.IsTrue(GeneralFunctions.WaitSameText(PAuth.TextUserNameSelector, Users.UserAli.Name, Wtime * 2),
                "�������� ��� ���������������� ������������");
        }


        [Test]
        [Description("�� ������������� �������������������� ������������")]
        public void NotAvtoryzedUser()
        {
            Step.Navigate(PMain, "������� �� ������� ��������");
            Step.Click(PMain.ButtonUserEntrance, "������ ������ ����� � �������");

            Step.SendKeys(PAuth.TextBoxEmail, Users.UserAli.Email, "����� ������������������ ����� � ���� ������");
            Step.SendKeys(PAuth.TextBoxPassword, "222222", "����� �������������������� ������ � ���� ������");
            Step.Click(PAuth.ButtonEntrance, "������ ������ ����� � ����� �����������");

            Assert.IsTrue(PAuth.TextError.Displayed, "�������� ��������� �� ������");
        }


        [Test]
        [Description("������� � ����������� �� �����������")]
        public void OpenRegistrationPage()
        {
            Step.Navigate(PMain, "������� �� ������� ��������");
            Step.Click(PMain.ButtonUserEntrance, "������ ������ ����� � �������");

            Step.Click(PAuth.ButtonRegistration, "������ ������ ������������������");

            Assert.IsTrue(PReg.TextBoxName.Displayed, "������� �������� �����������");
        }

        [Test]
        [Description("�������� �����������")]
        public void ChancelAuthoryzation()
        {
            Step.Navigate(PMain, "������� �� ������� ��������");
            Step.Click(PMain.ButtonUserEntrance, "������ ������ ����� � �������");

            Step.Click(PAuth.Button�ancel, "������ ������ ������");

            Assert.IsTrue(GeneralFunctions.WaitElementNotExist(PAuth.selectorPopupAuth), "������� ������� ��������");
        }


        [Test]
        [Description("������� �� �������� ������� �����������")]
        public void FacebookAuth()
        {
            Step.Navigate(PMain, "������� �� ������� ��������");
            Step.Click(PMain.ButtonUserEntrance, "������ ������ ����� � �������");

            Step.Click(PAuth.ButtonFacebookEntrance, "������ ������ ����� ����� �������");
            Step.WaitOpenWindow(Wtime, "��������� �������� ����");
            Step.SwitchWindow(driver.WindowHandles.Last(), "������� �� ����������� ��������");

            Assert.IsTrue(PFaceb.TextBoxEmail.Displayed, "������� �������� ����������� �������");
        }

        [Test]
        [Description("������� �� �������� ���� �����������")]
        public void GoogleAuth()
        {
            Step.Navigate(PMain, "������� �� ������� ��������");

            Step.Click(PMain.ButtonUserEntrance, "������ ������ ����� � �������");
            Step.Click(PAuth.ButtonGoogleEntrance, "������ ������ ����� ����� ����");
            Step.WaitOpenWindow(Wtime, "��������� �������� ����");
            Step.SwitchWindow(driver.WindowHandles.Last(), "������� �� ����������� ��������");

            Assert.IsTrue(PGgl.TextBoxEmail.Displayed, "������� �������� ����������� ����");
        }

        [Test]
        [Description("������������� ������������������ ������������ ����� ������ ������")]
        public void AvtoryzedUserPersonalCab()
        {
            Step.Navigate(PWish, "������� �� �������� ����� � ������ �������");

            Step.SendKeys(PAuth.TextBoxEmail, Users.UserAli.Email, "����� ������������������ ����� � ���� ������");
            Step.SendKeys(PAuth.TextBoxPassword, Users.UserAli.Password, "����� ������������������ ������ � ���� ������");
            Step.Click(PAuth.ButtonPersonalCabEntrance, "������ ������ �����");

            Assert.IsTrue(GeneralFunctions.WaitSameText(PAuth.TextUserNameSelector, Users.UserAli.Name, Wtime),
                "�������� ��� ���������������� ������������");
        }


        [Test]
        [Description("�� ������������� �������������������� ������������ ����� ������ ������")]
        public void NotAvtoryzedUserPersonalCab()
        {
            Step.Navigate(PWish, "������� �� �������� ����� � ������ �������");

            Step.SendKeys(PAuth.TextBoxEmail, Users.UserAli.Email, "����� ������������������ ����� � ���� ������");
            Step.SendKeys(PAuth.TextBoxPassword, "222222", "����� �������������������� ������ � ���� ������");
            Step.Click(PAuth.ButtonPersonalCabEntrance, "������ ������ �����");

            Assert.IsTrue(PAuth.TextError.Displayed, "�������� ��������� �� ������");
        }



    }
}

