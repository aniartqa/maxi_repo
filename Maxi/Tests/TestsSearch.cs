﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Поиск")]
    class TestsSearch<TBrowser> : TestBase<TBrowser> //Test Suite для проверки поиска товаров
    {
        [Test]
        [Description("Товар найден по полному названию товара")]
        public void SearchFullProductName()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.SendKeys(PMain.TextBoxSearch, Products.SomeProduct.ProductNameSpaces, "Ввели в поиск полное имя товара");
            Step.Click(PMain.ButtonSearch, "Нажали на кнопку поиск");

            Assert.AreEqual(Products.SomeProduct.ProductName, PSearch.TextProductNameString, 
                "Имя выбраного товара равно имени товара в результатах поиска");
        }

        [Test]
        [Description("Товар найден по короткому названию товара")]
        public void SearchShortProductName()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.SendKeys(PMain.TextBoxSearch, "Apple", "Ввели в поиск короткое имя товара");
            Step.Click(PMain.ButtonSearch, "Нажали на кнопку поиск");

            Assert.IsTrue(PSearch.TextProductName.Displayed,
                "Товар найден");
        }

        [Test]
        [Description("Товар найден по коду товара")]
        public void SearchProductCode()
        {
            Step.GetSomeProduct("Выбрали товар");

            Step.SomeProductPageOpen("Перешли на страницу выбранного товара");
            string ProductCode = (string)Step.GetValue(PProd.TextProductCode, "Запомнили код товара");
            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.SendKeys(PMain.TextBoxSearch, ProductCode, "Ввели в поиск код товара");
            Step.Click(PMain.ButtonSearch, "Нажали на кнопку поиск");

            Step.Click(PSearch.TextProductName, "Перешли на страницу найденого товара");
            Step.Wait(PProd.ButtonAllCredits, "Дождались появления карточки товара на странице товара");

            Assert.IsTrue(PProd.TextProductName.Contains(Products.SomeProduct.ProductName),
                "Имя выбраного товара равно имени товара");
        }

        [Test]
        [Description("Несуществующий товар не найден")]
        public void SearchNotExistProduct()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.SendKeys(PMain.TextBoxSearch, "4230948293dhhwuqhdu686686fhfhfg", "Ввели в поиск несуществующее имя товара");
            Step.Click(PMain.ButtonSearch, "Нажали на кнопку поиск");

            Assert.IsTrue(PSearch.TextNoResultSearch.Displayed,
                "Несуществующий товар не найден");
        }

      

    }
    


}
