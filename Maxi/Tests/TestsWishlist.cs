﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Лист желаний")]
    class TestsWishlist<TBrowser> : TestBase<TBrowser> //Test Suite для проверки листа желаний
    {
        [Test]
        [Description("Удален товар из листа желаний")]
        public void DeleteProductWishlist()
        {
            Step.Authoryzation("Авторизировались");
            Step.GetSomeProduct("Выбрали товар");
            Step.AddToWishList("Добавили товар в лист желаний");

            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.Click(PMain.ButtonWishlist, "Нажали на кнопку перехода к листу желаний");
            Step.Wait(PWish.ButtonWishlistGroop.Displayed, "Дождались появления листа желаний");

            Step.Click(PWish.ButtonDeleteItem, "Нажали на кнопку удаления товара в листе желаний");

            Assert.IsTrue(GeneralFunctions.WaitElementNotExist(PWish.TextCardProduct), 
                "На странице листа желаний нет товаров");
        }



        [Test]
        [Description("Товар из слайдера добавлен в лист желаний")]
        public void SliderAddWishlist()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsWishlist("Удалили все товары в листе желание");
            Step.Navigate(PMain, "Перешли на главную страницу");

            Step.MoveTo(PMain.SliderLider, "Пролистали страницу до слайдера лидеров просмотра");
            Step.Hover(PMain.CardProductSliderLider, "Навели курсор на карточку товара");

            string prodName = (string)Step.GetValue(PProdGroup.TextProductNameSlider, "Запомнили имя выбранного товара");
            Step.Click(PProdGroup.ButtonAddWishListSlider, "Нажали на кнопку добавления товара в лист желаний");
            Step.Wait(PWish.PopupChooseWishList, "Дождались появления окна выбора листа желаний");
            Step.Click(PWish.ButtonChooseWishList, "Нажали на название первого из листов желаний");
            Step.Click(PWish.ButtonAddWishList, "Нажали кнопку добавления товара в выбранный лист желаний");
            Step.Wait(PWish.PopupProductAddedWishList, "Дождались появления сообщения об успешном добавлении товара в лист желаний");

            Step.Navigate(PWish, "Перешли на страницу листа желаний");

            Assert.AreEqual(PWish.TextProductName, prodName,
                "Название товара в листе желаний равно названию выбранного товара");
        }


        [Test]
        [Description("Товар из группы товаров добавлен в лист желаний")]
        public void ProductGroupAddWishlist()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsWishlist("Удалили все товары в листе желание");
            Step.Navigate(PProdGroup.UrlFridge, "Выбрали товар из группы товаров холодильники");

            string prodName = (string)Step.GetValue(PProdGroup.TextProductName, "Запомнили имя выбранного товара");
            Step.Hover(PProdGroup.TextCardProduct, "Навели курсор на карточку товара");

            Step.Click(PProdGroup.ButtonAddWishList, "Нажали на кнопку добавления товара в лист желаний");
            Step.Wait(PWish.PopupChooseWishList, "Дождались появления окна выбора листа желаний");
            Step.Click(PWish.ButtonChooseWishList, "Нажали на название первого из листов желаний");
            Step.Click(PWish.ButtonAddWishList, "Нажали кнопку добавления товара в выбранный лист желаний");
            Step.Wait(PWish.PopupProductAddedWishList, "Дождались появления сообщения об успешном добавлении товара в лист желаний");

            Step.Navigate(PWish, "Перешли на страницу листа желаний");

            Assert.AreEqual(PWish.TextProductName, prodName,
                "Название товара в листе желаний равно названию выбранного товара");
        }

        [Test]
        [Description("Товар из страницы товара добавлен в лист желаний")]
        public void ProductPageAddWishlist()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsWishlist("Удалили все товары в листе желание");
            Step.GetAnotherProduct("Выбран товар");
            Step.AnotherProductPageOpen("Открыта страница товара");

            string prodName = Products.AnotherProduct.ProductName;

            Step.Click(PProdGroup.ButtonAddWishListProductPage, "Нажали на кнопку добавления товара в лист желаний");
            Step.Wait(PWish.PopupChooseWishList, "Дождались появления окна выбора листа желаний");
            Step.Click(PWish.ButtonChooseWishList, "Нажали на название первого из листов желаний");
            Step.Click(PWish.ButtonAddWishList, "Нажали кнопку добавления товара в выбранный лист желаний");
            Step.Wait(PWish.PopupProductAddedWishList, "Дождались появления сообщения об успешном добавлении товара в лист желаний");

            Step.Navigate(PWish, "Перешли на страницу листа желаний");

            Assert.IsTrue(prodName.Contains(PWish.TextProductName),
                "Название товара в листе желаний равно названию выбранного товара");
        }


        [Test]
        [Description("Товар из листа желаний добавлен в корзину")]
        public void CartAddWishList()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.DeleteAllProductsWishlist("Удалили все товары в листе желаний");
            Step.GetSomeProduct("Выбрали товар");
            Step.AddToWishList("Добавили товар в лист желаний");
            Step.Navigate(PWish,"Перешли на страницу листа желаний");

            Step.Click(PWish.ButtonByWishlist, "Нажали на кнопку купить товар");
            Step.Wait(PMain.BasketProd, "Дождались появления сообщения о добавлении в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");

            Assert.IsTrue(PCart.Product.Displayed, "Выбранный товар добавлен в корзину");
        }


        [Test]
        [Description("Создан и удален лист желаний")]
        public void AddDeleteWishlist()
        {
            Step.Authoryzation("Авторизировались");
            Step.Navigate(PWish, "Перешли на страницу листа желаний");

            Step.Click(PWish.ButtonCreateWishlist, "Нажали на кнопку перехода к листу желаний");
            Step.SendKeys(PWish.TextFieldListName, "List1", "Нажали на кнопку перехода к листу желаний");
            Step.Click(PWish.ButtonFormCreateWishlist, "Нажали на кнопку перехода к листу желаний");
            Step.Wait(GeneralFunctions.WaitElementNotExist(PWish.TextFieldListNameSelector), 
                "Нажали на кнопку удаления товара в листе желаний");
            Step.Click(PWish.ButtonOpenWishlist, "Дождались появления листа желаний");
            Step.Click(PWish.ButtonChooseWishlist, "Дождались появления листа желаний");

            Step.Wait(PWish.TextSelectedWishlist, "Нажали на кнопку удаления товара в листе желаний");
            Step.Wait(GeneralFunctions.WaitSameText(PWish.TextSelectedWishlistSelector, "List1", Wtime), 
                "Нажали на кнопку удаления товара в листе желаний");
            Step.Click(PWish.ButtonDeleteWishlist, "Дождались появления листа желаний");

            Assert.IsTrue(GeneralFunctions.WaitSameText(PWish.TextSelectedWishlistSelector, "WishList", Wtime), 
                "Нажали на кнопку удаления товара в листе желаний");
        }
    
    
    }

}
