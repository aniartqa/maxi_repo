﻿using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Фильтры")]
    class TestsFilters<TBrowser> : TestBase<TBrowser> //Test Suite для проверки фильтров товаров
    {

        [Test]
        [Description("Товары отфильтрованы по производителю")]
        public void FilterProductsBrandSmartphone()
        {
            Step.Authoryzation("Авторизировались");

            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.Click(PMain.ButtonGroupPhone, "Нажали на кнопку меню");
            Step.Wait(PMain.PopupMenuActive, "Дождались открытия меню");
            Step.Click(PMain.ButtonSubGroupSmartphone, "Нажали на кнопку группы в меню");

            Step.Wait(PProdGroup.FormFilters, "Дождались открытия странцы группы товаров");

            Step.Click(PProdGroup.ButtonFilterCharacterBrand,
                "Выбрали фильтр по первому производителю в списке производителей");
            string characterName = (string)Step.GetValue(PProdGroup.TextFilterCharacterBrand,
                "Запомнили название выбранного производителя");
            Step.Wait(PProdGroup.ButtonFilterCharacterActive, "Дождались отфильтрованой страницы");
            Step.Click(PProdGroup.ButtonProductName, "Перешли на страницу товара-результата поиска");

            Assert.AreEqual(PProd.TextCharacterType, characterName,
                "Название производителя товара на странице товара равно названию выбранного производителя");
        }


        //[Test]
        //[Description("Товары отфильтрованы по размеру диагонали")]
        //public void FilterProductsScreenSizeTV()
        //{
        //    Step.Navigate(PProdGroup.UrlTelevisor, "Выбрали товар из группы товаров телевизоры");
        //    Step.Click(PProdGroup.ButtonFilterCharacterScreen,
        //        "Выбрали фильтр по первому размеру диагонали в списке размеров диагонали");
        //    string characterName = (string)Step.GetValue(PProdGroup.TextFilterCharacterScreen,
        //        "Запомнили название выбранного размера диагонали");
        //    Step.Wait(PProdGroup.ButtonFilterCharacterActive, "Дождались отфильтрованой страницы");
        //    Step.Click(PProdGroup.ButtonProductName, "Перешли на страницу товара-результата поиска");
        //    Step.Click(PProd.ButtonAllCharacters, "Нажали на кнопку Показать все характеристики");

        //    var a = characterName;
        //    var v = PProd.TextCharacterTypeScreenSize;

        //    Assert.IsTrue(characterName.Contains(PProd.TextCharacterTypeScreenSize),
        //        "Название размера диагонали товара на странице товара равно названию выбранного размера");
        //}

        //[Test]
        //[Description("Товары отфильтрованы по энергопотреблению")]
        //public void FilterProductsEnergyFridge()
        //{
        //    Step.Navigate(PProdGroup.UrlFridge, "Выбрали товар из группы товаров телевизоры");
        //    Step.Click(PProdGroup.ButtonFilterCharacterEnergy,
        //        "Выбрали фильтр по первому размеру диагонали в списке размеров диагонали");
        //    string characterName = (string)Step.GetValue(PProdGroup.TextFilterCharacterEnergy,
        //        "Запомнили название выбранного размера диагонали");
        //    Step.Wait(PProdGroup.ButtonFilterCharacterActive, "Дождались отфильтрованой страницы");
        //    Step.Click(PProdGroup.ButtonProductName, "Перешли на страницу товара-результата поиска");
        //    Step.Click(PProd.ButtonAllCharacters, "Нажали на кнопку Показать все характеристики");

        //    var a = characterName;
        //    var v = PProd.TextCharacterTypeScreenSize;

        //    Assert.IsTrue(characterName.Contains(PProd.TextCharacterTypeEnergy),
        //        "Название размера диагонали товара на странице товара равно названию выбранного размера");
        //}



        [Test]
        [Description("Товары отсортированы по убыванию цены")]
        public void SortPriceProductsDescending()
        {
            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");
            Step.Click(PProdGroup.ButtonPriceSort, "Выбрали сортировку по убыванию цены");
            Step.Wait(PProdGroup.ButtonPriceSortActive, "Дождались отсортированой страницы");

            Assert.IsTrue(PProdGroup.TextProductPriceFirst >= PProdGroup.TextProductPriceSecond,
                "Значение цены первого по счету в списке товаров товара больше или равно значению второго товара");
        }

        [Test]
        [Description("Товары выбраны по диапазону цены")]
        public void SortPriceProductsRoll()
        {
            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");
            double price = (double)Step.GetValue(PProdGroup.TextProductPriceSecond, 
                "Запомнили значение цены второго товара в списке товаров");

            Step.CleanAndSendTextField(PProdGroup.TextBoxPriceRollFirst, price.ToString(), 
                "Введена цена второго товара на странице в поле ограничения минимальной цены");
            Step.Wait(PProdGroup.ButtonRollFilterActive, "Дождались отсортированой страницы");
             string text = (string)Step.GetValue(PProdGroup.ButtonRollFilterActive.Text, 
                 "Запомнили значение текста на иконке активного фильтра");

            Step.CleanAndSendTextField(PProdGroup.TextBoxPriceRollSecond, price.ToString(), 
                "Введена цена второго товара на странице в поле ограничения максимальной цены");
            Step.WaitTextChange(PProdGroup.ButtonRollFilterActiveSelctor, text, Wtime, 
                "Дождались изменения текста на иконке активного фильтра");
            
            Assert.IsTrue(PProdGroup.TextProductPriceFirst == price, 
                "Цена первого товара на странице равна введеной в поля ограничения цене");
        }


    }


}