﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework
{
    
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("API")]
    class TestsApi<TBrowser> : TestBase<TBrowser> //Test Suite для проверки API запросов высокого приоритета
    {
        [Test]
        [Description("Авторизирован пользователь через API")]
        public void ApiAuthorization()
        {
            Step.Authoryzation("Выполнили запрос на авторизацию");
            bool noErrors = Step.ApiError(Apis.ResultAuthorization, "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResult = Step.ApiResultCorrect(Apis.ResultAuthorization, "form-identification-form",
                "Проверели что результат запроса содержит форму из личного кабинета пользователя");

            Assert.IsTrue(noErrors && correctResult, "Авторизирован пользователь");
        }

        [Test]
        [Description("Добавлены и удалены товары в листе желаний через API")]
        public void ApiAddAndDeleteProductWishlist()
        {
            Step.Authoryzation("Авторизировались");
            Step.GetSomeProduct("Выбрали товар");
            Step.DeleteAllProductsWishlist("Выполнили запрос на удаление всех товаров в листе желаний");

            Step.AddToWishList("Выполнили запрос на добавление товара в лист желаний");
            Step.DeleteAllProductsWishlist("Выполнили запрос на удаление всех товаров в листе желаний");

            bool noErrorsAddToWishList = Step.ApiError(Apis.ResultAddProductWishlist,
                "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResultAddToWishList = Step.ApiResultCorrect(Apis.ResultAddProductWishlist, "js_success",
                "Проверели что результат запроса содержит сообщение о добавлении выбраного товара в лист желаний");

            bool noErrorsDeleteAllProductsWishlist = Step.ApiError(Apis.ResultDeleteAllProductsWishlist,
                "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResultDeleteAllProductsWishlist = Step.ApiFall(Apis.ResultDeleteAllProductsWishlist, Apis.GetShortProductName(),
                "Проверели что результат запроса не содержит имени добавленого в лист желаний товара");

            Assert.IsTrue(noErrorsDeleteAllProductsWishlist && correctResultDeleteAllProductsWishlist && noErrorsAddToWishList && correctResultAddToWishList, 
                "Добавлены и удалены товары в листе желаний через API");
        }


        [Test]
        [Description("Удалены товары в корзине через API")]
        public void ApiDeleteAllProductsCart()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");

            Step.DeleteAllProductsInCart("Выполнили запрос на удаление всех товаров в корзине");
            bool noErrors = Step.ApiError(Apis.ResultDeleteAllProductsCart, "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResult = Step.ApiResultCorrect(Apis.ResultDeleteAllProductsCart, "empty_basket",
                "Проверели что результат запроса содержит контейнер пустой корзины");

            Assert.IsTrue(noErrors && correctResult, "Удалены товары в корзине через API");
        }

        [Test]
        [Description("Добавлен товар в корзину через API")]
        public void ApiAddProductToCart()
        {
            Step.GetSomeProduct("Выбрали товар");

            Step.AddSomeProductCart("Выполнили запрос на добавление товара в корзину");
            bool noErrors = Step.ApiError(Apis.ResultAddProductToCart, "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResult = Step.ApiResultCorrect(Apis.ResultAddProductToCart, @"product"":{""price",
                "Проверели что результат запроса содержит выбраный товар в корзине");
            
            Assert.IsTrue(noErrors && correctResult, "Добавлен товар в корзину через API");
        }

        [Test]
        [Description("Добавлен товар в сравнение через API")]
        public void ApiAddCompare()
        {
            Step.GetSomeProduct("Выбрали товар");

            Step.AddSomeProductCompere("Выполнили запрос на добавление товара в сравнение через API");
            bool noErrors = Step.ApiError(Apis.ResultAddToCompare, "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResult = Step.ApiResultCorrect(Apis.ResultAddToCompare, "products_compare_small", 
                "Проверели что результат запроса содержит таблицу сравнения");

            Assert.IsTrue(noErrors && correctResult, "Добавлен товар в сравнение через API");
        }

        [Test]
        [Description("Добавлен товар в оформление покупки через API")]
        public void ApiAddCheckIn()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");

            Step.AddToCheckIn("Выполнили запрос на добавление товара в соформление покупки через API");
            bool noErrors = Step.ApiError(Apis.ResultAddToCheckIn, "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResult = Step.ApiResultCorrect(Apis.ResultAddToCheckIn, Apis.GetShortProductName(), 
                "Проверели что результат запроса содержит заказ с выбраным товаром");

            Assert.IsTrue(noErrors && correctResult, "Добавлен товар в оформление покупки через API");
        }

        [Test]
        [Description("Выбран доступный к продаже товар через API")]
        public void ApiGetSomeProduct()
        {
            Step.GetSomeProduct("Выполнили запрос на получение доступного к продаже товара через API");
            bool noErrors = Step.ApiError(Apis.ResultSomeExistProduct, "Проверели что результат запроса не содержит сообщений об ошибках");
            bool correctResult = Step.ApiResultCorrect(Apis.ResultSomeExistProduct, "one-cat-item", 
                "Проверели что результат запроса содержит выбранный существующий товар");

            Assert.IsTrue(noErrors && correctResult, "Выбран доступный к продаже товар через API");
        }

    }
}
