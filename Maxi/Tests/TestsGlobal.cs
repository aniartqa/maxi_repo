﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Общие функции")]
    class TestsGlobal<TBrowser> : TestBase<TBrowser>//Test Suite для проверки общих функций
    {
        //[Test]
        //[Description("Получение данных о скорости загрузки страницы")]
        ////Полученые данные хранятся в папке bin проекта в файле с названием в виде текущей даты 
        //public void TestSpeed()
        //{
        //    Performance.GetDataPerformance("https://natalibolgar.com/", "Получили данные о скорости загрузки страницы");
        //    Performance.GetDataPerformance("https://natalibolgar.com/catalog/platya/", "Получили данные о скорости загрузки страницы");
        //    Performance.GetDataPerformance("https://natalibolgar.com/nashy_trendy/30-gotovykh-obrazov-dlya-moroznykh-dney/", "Получили данные о скорости загрузки страницы");
        //    Performance.GetDataPerformance("https://natalibolgar.com/catalog/topy/sizes-40/", "Получили данные о скорости загрузки страницы");
        //    Performance.GetDataPerformance("https://natalibolgar.com/catalog/obuv_i_sumki/botinki/", "Получили данные о скорости загрузки страницы");
        //    Performance.GetDataPerformance("https://natalibolgar.com/catalog/verkhnyaya_odezhda/", "Получили данные о скорости загрузки страницы");
        //    Performance.GetDataPerformance("https://natalibolgar.com/catalog/verkhnyaya_odezhda/parka_pukhovik_chernogo_tsveta-v-65-ny-0001.html", "Получили данные о скорости загрузки страницы");
           
        //}


        [Test]
        [Description("Выведены еще 24 товара")]
        public void ShowMoreProducts()
        {
            Step.NavigateOnPage(PProdGroup.SecondPage, "Перешли на страницу номер 2 в группе товаров");
            string productNameOnSecondPage =
                (string)Step.GetValue(GeneralFunctions.NoSpaces(PProdGroup.ButtonProductName.Text), 
                "Записали имя первого товара на странице номер 2");

            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");
            Step.Click(PProdGroup.ButtonShowMore, "Нажали на кнопку Показать еще 24 товара");
            Step.Wait(PProdGroup.ButtonPadingTwoActive, "Дождались изменения количества товаров на странице");

            Assert.AreEqual(productNameOnSecondPage, GeneralFunctions.NoSpaces(PProdGroup.TextListProducts[24].Text), 
                "Товар на странице номер 2 выведен на текущей странице");
        }

        [Test]
        [Description("Открыта следующая страница в каталоге товаров")]
        public void Pading()
        {
            Step.NavigateOnPage(PProdGroup.SecondPage, "Перешли на страницу номер 2 в группе товаров");
            string productNameOnSecondPage =
                (string)Step.GetValue(GeneralFunctions.NoSpaces(PProdGroup.ButtonProductName.Text), 
                "Записали имя первого товара на странице номер 2");

            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");
            Step.Click(PProdGroup.ButtonPadingTwo, "Нажали кнопку перехода на страницу номер два");
            Step.Wait(PProdGroup.ButtonPadingTwoActive, "Дождались открытия страницы");

            Assert.AreEqual(productNameOnSecondPage, GeneralFunctions.NoSpaces(PProdGroup.ButtonProductName.Text), "Товар на странице номер 2 выведен на текущей странице");
        }


        [Test]
        [Description("Список товаров выведен горизонтально")]
        public void ChangeProductListSow()
        {
            Step.Authoryzation("Авторизировались");
            Step.Navigate(PProdGroup, "Перешли на страницу группы товаров");

            Step.Click(PProdGroup.ButtonChangeShow, "Нажали на кнопку горизонтального вывода списка товаров");

            Assert.IsTrue(PProdGroup.TextChangeShow.Displayed, "Список товаров выведен горизонтально");
        }

        [Test]
        [Description("Открыта страница Maxi скидки")]
        //Тест покрывает переодически возникающий баг с некликабельностью кнопки Показать все скидки
        public void OpenMaxiDiscountPage()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.Click(PMain.ButtonMaxiDiscount, "Нажали на кнопку Показать все скидки");

            Assert.IsTrue(PProdGroup.ButtonMaxiDiscountGroop.Displayed, "Открыта страница Maxi скидки");
        }

        [Test]
        [Description("Открыта страница Новинки")]
        //Тест покрывает переодически возникающий баг с некликабельностью кнопки Показать еще Новинки
        public void OpenNoveltyPage()
        {
            Step.Authoryzation("Авторизировались");

            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.MoveTo(PMain.SliderNew, "Пролистали страницу до слайдера новинки");
            Step.Hover(PMain.CardProductSliderNew, "Навели курсор на карточку товара");
            Step.Hover(PMain.ButtonNovelty, "Навели курсор на карточку товара");
            Step.Click(PMain.ButtonNovelty, "Нажали на кнопку Показать еще Новинки");

            Assert.IsTrue(PProdGroup.ButtonNoveltyGroop.Displayed, "Открыта страница Новинки");
        }

        [Test]
        [Description("Локализация изменена")]
        //Тест покрывает переодически возникающий баг с неизменяемостью языка
        public void ChangeLocalization()
        {
            Step.Navigate(PMain, "Перешли на главную страницу");
            string word = (string)Step.GetValue(PMain.ButtonSearch.GetAttribute("value"),"Запомнили значение слова");
            Step.Click(PMain.ButtonLocalization, "Нажали на кнопку изменения языка");
            Step.Wait(PMain.TextLocalizationChanged,"Дождались активации кнопки азербайджанского языка");

            Assert.AreNotEqual(word, (string) PMain.ButtonSearch.GetAttribute("value"),
                "Первоначальное значение слова отличаеться от теперешнего");
        }
    }
}
