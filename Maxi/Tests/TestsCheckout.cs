﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Pages.Authorythation;
using TestFramework.Tests;

namespace TestFramework
{
    [TestFixture(typeof(ChromeDriver))]
    //[TestFixture(typeof(string))]
    [Category("Оформление заказа")]
    class TestsCheckout<TBrowser> : TestBase<TBrowser> //Test Suite для проверки оформления товара
    {
        [Test]
        [Description("Перешли к оформлению заказа из мини-корзины")]
        public void CheckoutCardSmallMenu()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");

            Step.Navigate(PMain, "Перешли на главную страницу");
            Step.Hover(PMain.ButtonCart, "Навели курсор на иконку корзины");
            Step.Click(PMain.ButtonCheckout, "Нажали на кнопку оформления покупки");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Assert.AreEqual(Products.SomeProduct.ProductName, PCheck.TextProductName,
                            "Название товара в сравнении равно названию выбраного товара");

        }

        [Test]
        [Description("Перешли к оформлению заказа из popup добовления товара в корзину")]
        public void CheckoutProductAddCartPopup()
        {
            Step.GetSomeProduct("Выбрали товар");
            Step.SomeProductPageOpen("Открыта страница выбранного товара");

            Step.Click(PProd.ButtonBy, "Нажали на кнопку купить товар");
            Step.Wait(PProd.BasketProd, "Дождались появления сообщения о добавлении в корзину");
            Step.Click(PProd.ButtonCheckoutForm, "Нажали на кнопку оформления покупки");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Assert.AreEqual(Products.SomeProduct.ProductName, PCheck.TextProductName,
                            "Название товара в checkout равно названию выбраного товара");
        }

        [Test]
        [Description("Перешли к оформлению заказа из корзины")]
        public void CheckoutCard()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.GetSomeProduct("Выбрали товар");
            Step.AddSomeProductCart("Добавили товар в корзину");

            Step.Navigate(PCart, "Перешли на страницу корзины");
            Step.Click(PCart.ButtonCheckoutCart, "Нажали на кнопку оформления покупки");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Assert.AreEqual(Products.SomeProduct.ProductName, PCheck.TextProductName,
                            "Название товара в сравнении равно названию выбраного товара");
        }

        [Test]
        [Description("Полностью оформлен заказ товара зарегистрированным пользователем")]
        public void CheckoutFullAuth()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.GetSomeProductMouse("Выбрали товар из раздела выпечки");
            Step.AddSomeProductCart("Добавили товар в корзину");
            Step.AddToCheckIn("Добавили товары в корзине в оформление заказа");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Step.Click(PCheck.ButtonNextStep1, "Нажали на кнопку Следующий шаг");
            Step.Click(PCheck.ButtonAdditionalOrder, "Нажали на кнопку добавления комментария к заказу");
            Step.SendKeys(PCheck.TextFieldAdditionalOrder, "test", "Добавили комментарий test к заказу");
            Step.Click(PCheck.ButtonNextStep2Step3, "Нажали на кнопку Следующий шаг");
            Step.Wait(PCheck.ButtonPayment, "Дождались появления выбора оплаты");
            Step.Click(PCheck.ButtonNextStep2Step3, "Нажали на кнопку Следующий шаг");
            Step.Wait(PCheck.TextBonus, "Дождались появления формы Bonus+");
            Step.Click(PCheck.ButtonConfirmOrder, "Оформили заказ");

            Step.Wait(PCheck.ButtonPersonalCab, "Дождались появления кнопки Личный кабинет");
            Step.Navigate(PComplOrd, "Перешли на страницу оформленных заказов");
            Step.Click(PComplOrd.ButtonShowOrder, "Нажали кнопку Показать содержимое заказа");
            string productName = (string)Step.GetValue(PComplOrd.TextProductNameString,
                    "Запомнили название товара в оформленном заказе");
            Step.Click(PComplOrd.ButtonChancelOrder, "Нажали кнопку отмены заказа");
            Step.SendKeys(PComplOrd.TextBoxReasonChancelOrder, "test", "Добавили комментарий test в причине отмены заказа");
            Step.Click(PComplOrd.ButtonPopupChancelOrder, "Отменили заказ");

            Assert.IsTrue(productName.Contains(Products.SomeProduct.ProductName, StringComparison.InvariantCultureIgnoreCase),
                            "Название товара в оформленном равно названию выбраного товара");
        }


        [Test]
        [Description("Вошли в аккаунт пользователя из оформления заказа")]
        public void CheckoutAuthorization()
        {
            Step.GetSomeProductMouse("Выбрали товар из раздела выпечки");
            Step.AddSomeProductCart("Добавили товар в корзину");
            Step.AddToCheckIn("Добавили товары в корзине в оформление заказа");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Step.Click(PCheck.ButtonEntranceAccount, "Нажали на кнопку Я постоянный клиент");
            Step.SendKeys(PCheck.TextBoxEmail, Users.UserAli.Email, "Ввели email");
            Step.SendKeys(PCheck.TextBoxPass, Users.UserAli.Password, "Ввели пароль");
            Step.Click(PCheck.ButtonEntrance, "Нажали на кнопку Ввойти");

            Assert.IsTrue(PCheck.TextBoxUserPhone.Displayed,
                            "Выведен номер телефона пользователя");
        }


        [Test]
        [Description("Полностью оформлен заказ товара зарегистрированным пользователем")]
        public void CheckoutSumProducts()
        {
            Step.GetSomeProduct("Выбрали первый товар");
            Step.AddSomeProductCart("Добавили первый товар в корзину");
            Step.GetAnotherProduct("Выбран второй товар");
            Step.AddAnotherProductCart("Добавили второй товар в корзину");
            Step.AddToCheckIn("Добавили товары в корзине в оформление заказа");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Assert.AreEqual(Products.SomeProduct.ProductPrice + Products.AnotherProduct.ProductPrice, PCheck.TextSum,
                            "Название товара в оформленном равно названию выбраного товара");
        }


        [Test]
        [Description("Полностью оформлен заказ товара зарегистрированным пользователем")]
        public void CheckoutFullPayCard()
        {
            Step.Authoryzation("Авторизировались");
            Step.DeleteAllProductsInCart("Удалили все товары в корзине");
            Step.GetSomeProductMouse("Выбрали товар из раздела выпечки");
            Step.AddSomeProductCart("Добавили товар в корзину");
            Step.AddToCheckIn("Добавили товары в корзине в оформление заказа");
            Step.Wait(PCheck.ButtonMenuSteps, "Дождались перехода на страницу оформления заказа");

            Step.Click(PCheck.ButtonNextStep1, "Нажали на кнопку Следующий шаг");
            Step.Click(PCheck.ButtonAdditionalOrder, "Нажали на кнопку добавления комментария к заказу");
            Step.SendKeys(PCheck.TextFieldAdditionalOrder, "test", "Добавили комментарий test к заказу");
            Step.Click(PCheck.ButtonNextStep2Step3, "Нажали на кнопку Следующий шаг");
            Step.Click(PCheck.CheckBoxPayCard, "Нажали на кнопку оплаты картой online");
            Thread.Sleep(3000);// пока не пофиксят баг с неработающей 3 секунды кнопкой
            Step.Click(PCheck.ButtonNextStep2Step3, "Нажали на кнопку Следующий шаг");
            Step.Wait(PCheck.TextBonus, "Дождались появления формы Bonus+");
            Step.Click(PCheck.ButtonConfirmOrder, "Оформили заказ");
            Assert.IsTrue(PCheck.ButtonPayCard.Displayed, "Выведено окно оплаты картой");

            Step.Wait(PCheck.ButtonPersonalCab, "Дождались появления кнопки Личный кабинет");
            Step.Navigate(PComplOrd, "Перешли на страницу оформленных заказов");
            Step.Click(PComplOrd.ButtonShowOrder, "Нажали кнопку Показать содержимое заказа");
            Step.Wait(PComplOrd.TextProductName,
                    "Запомнили название товара в оформленном заказе");
            Step.Click(PComplOrd.ButtonChancelOrder, "Нажали кнопку отмены заказа");
            Step.SendKeys(PComplOrd.TextBoxReasonChancelOrder, "test", "Добавили комментарий test в причине отмены заказа");
            Step.Click(PComplOrd.ButtonPopupChancelOrder, "Отменили заказ");
        }
    }
}
