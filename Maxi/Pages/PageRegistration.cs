﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages
{
    public class PageRegistration : PageBase
    {
        public IWebElement TextBoxName => WaitElement( By.XPath("//input[@name='REGISTER[NAME]']"), Wtime);

        public PageRegistration() { Url = ""; }
    }
}
