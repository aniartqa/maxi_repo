﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageMain : PagePattern//главная страница
    {
        public PageMain() { Url = ""; }


        public IWebElement ButtonBySliderSale => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'maxi-sale')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//a[contains(@class,'by-item')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Купить в слайдере скидки на главной странице 
        public IWebElement CardProductSliderSale => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'maxi-sale')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//отображение карточки товара в слайдере скидки на главной странице 
        public IWebElement SliderSale => WaitElement(By.XPath("" +
            "//div[contains(@class,'maxi-sale')]"), Wtime);//отображение слайдера скидки на главной странице 


        public IWebElement ButtonBySliderLider => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'most-view')][2]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//a[contains(@class,'by-item')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Купить слайдера лидеры просмотротров на главной странице 
        public IWebElement ButtonCompareSliderLider => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'most-view')][2]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//div[contains(@class,'do-item')]//a[contains(@class,'comp')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Добавить в сравнение слайдера лидеры просмотров на главной странице 
        public IWebElement ButtonCompareSliderLiderActive => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'most-view')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//div[contains(@class,'do-item')]//a[contains(@class,'comp added')]"),
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//активированная кнопка Добавить в сравнение слайдера лидеры просмотров на главной странице 
        public IWebElement CardProductSliderLider => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'most-view')][2]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//отображение карточки товара слайдера лидеры просмотров на главной странице 
        public IWebElement SliderLider => WaitElement(By.XPath("//div[contains(@class,'most-view')][2]"), Wtime);//отображение слайдера лидеры просмотров на главной странице 


        public IWebElement ButtonBySliderNew => WaitElementNoFullLoad(By.XPath("//div[contains(@class,'carus-news _index')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//a[contains(@class,'by-item')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Кипить слайдера новинки на главной странице 
        public IWebElement CardProductSliderNew => WaitElementNoFullLoad(By.XPath("//div[contains(@class,'carus-news _index')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//отображение карточки товара в слайдере новинки на главной странице 
        public IWebElement SliderNew => WaitElement(By.XPath("//div[contains(@class,'carus-news _index')]"), Wtime);//отображение слайдера новинки на главной странице 


        public IWebElement ButtonBySliderDaySell => WaitElement(By.XPath("//div[contains(@class,'one-prod-item')]//a[contains(@class,'by-item')]"),
            Wtime);//кнопка Купить баннера Скидка дня на главной странице 
        public IWebElement ButtonCompare => WaitElement(By.XPath("//a[contains(@href,'compare') and contains(@class,'order-bt')]"),
            Wtime);//кнопка перехода к сравнению на главной странице 
        public IWebElement ButtonDeleteCompares => WaitElement(By.XPath("//a[contains(@id,'compare_clear_all')]"),
            Wtime);//кнопка удаления всех сравнений на главной странице 
        public IWebElement ImageCompare => WaitElement(By.XPath("//div[@id='products_compare_small']"),
            Wtime);//картинка-иконка сравнения
        public IWebElement TextEmptyCompare => WaitElement(By.XPath("//div[contains(@class,'hide-funct')]/p/br"),
            Wtime, new Condition(GeneralFunctions.Exist));//отображение текста отсутствия сравнений
        public IWebElement SliderHome => WaitElement(By.XPath("//div[@id='indexHomeSlider']"), Wtime);//отображение баннера Скидка дня на главной странице 

        public IWebElement ButtonNovelty => WaitElement(By.XPath("//div[contains(@class,'news-more')]//a[contains(@href,'novelty')]//span"), 
            Wtime);//кнопка Показать еще новинки на главной странице 
        public IWebElement ButtonMaxiDiscount => WaitElement(By.XPath("//div[contains(@class,'maxi-bann')]//a[contains(@href,'maxi-discount')]//span"), 
            Wtime);//кнопка Показать все скидки на главной странице 


        public IWebElement TextLocalizationChanged => WaitElement(By.XPath("//input[@value='Lang' and contains(@data-url,'ru')]"),
            Wtime, new Condition(GeneralFunctions.Exist));

        public IWebElement ButtonAddWishList => WaitElement(By.XPath("//input[@value='Lang' and contains(@data-url,'ru')]"),
           Wtime);
    }
}
