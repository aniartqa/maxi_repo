﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;
using TestFramework.Pages;

namespace Maxi.Pages
{
    public class PageCompletedOrders : PagePattern
    {

        public PageCompletedOrders() { Url = "personal/order/"; }

        public IWebElement TextProductName => WaitElement(By.XPath("//div[contains(@class,'tit-your-item-order')]//a"),
           Wtime);
        public string TextProductNameString => GeneralFunctions.NoSpaces(TextProductName.Text);
        public IWebElement ButtonChancelOrder => WaitElement(By.XPath("//a[contains(@class,'js_cancel_order')]"),
           Wtime);
        public IWebElement ButtonShowOrder => WaitElement(By.XPath("//a[contains(@href,'detail') and contains(@class,'show-order')]"),
           Wtime);
        public IWebElement TextBoxReasonChancelOrder => WaitElement(By.XPath("//textarea[@name='REASON_CANCELED']"),
           Wtime*2);
        public IWebElement ButtonPopupChancelOrder => WaitElement(By.XPath("//input[contains(@class,'js_submit_cancel_order')]"),
           Wtime);
        public IWebElement ButtonMyOrders => WaitElement(By.XPath("//aside//a[contains(@href,'personal/order/')]"),
          Wtime);


    }
}
