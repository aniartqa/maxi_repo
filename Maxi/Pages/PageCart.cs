﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageCart: PagePattern //страница корзины
    {
        public PageCart() { Url = "cart/"; }
        public IWebElement Product => WaitElement(ProductSelector, Wtime);//товар в корзине
        public By ProductSelector => By.XPath("//div[@product_id]");//css селектор товара в корзине

        
        public IWebElement ButtonBack => WaitElement(By.XPath(
            "//a[contains(@class,'cart_back')]"), Wtime);//кнопка Вернуться назад к покупкам в корзинне
        public IWebElement ButtonCountPluss => WaitElement(By.XPath(
            "//a[contains(@class,'plus jsChangeProduct')]"), Wtime);//кнопка увеличения числа товаров в корзинне
        public IWebElement ButtonDeleteFirstProduct => WaitElement(By.XPath(
            "//a[contains(@class,'cell-delete jsDelProduct')]//img"), Wtime);//кнопка удаления товара из корзины

        public double NumberSum => double.Parse(GeneralFunctions.LeftNumbers(TextSum.Text), 
            System.Globalization.CultureInfo.InvariantCulture);//число - сумма товаров в корзинне

        public IWebElement TextProductPrice => WaitElement(By.XPath(
            "//div[@product_id]//span[contains(@class,'cart_actual_price')]"), Wtime);//отображение цены товара в корзинне
        public IWebElement TextProductName => WaitElement(By.XPath("" +
            "//div[@product_id]//a[contains(@class,'item-name')]"), Wtime);//отображение имени товара в корзинне
        public IWebElement TextSum => WaitElement(By.XPath("" +
            "//div[contains(@class,'line-price-in')]"), Wtime);//отображение суммы товаров в корзине
        public By TextSumSelector => By.XPath("" +
            "//div[contains(@class,'line-price-in')]");//css селектор сумы товаров в корзине
        public IWebElement TextEmptyCart => WaitElement(TextBoxEmptyCartSelector, Wtime*2);//Отображение текста "Корзина пуста" в корзине
        public By TextBoxEmptyCartSelector => By.XPath("" +
            "//div[contains(@class,'empty_basket')]");//css селектор текста "Корзина пуста"


        public List<IWebElement> TextSaleCart => ((driver.FindElements(By.XPath("" +
            "//span[contains(@class,'cart_economy')]")))).ToList();//отображение "Скидка в корзине"
        public List<double> NumbersSaleCart => FillEmptyList(new List<double> {0, 0}, TextSaleCart);//список отображений "Скидка в корзине" (для каждого товара в корзине)


        public IWebElement ButtonCheckoutCart => WaitElement(By.XPath(
            "//input[contains(@onclick, 'checkOut')]"), Wtime);

        /// <summary>
        /// Ждет пока в корзине измениться сумма
        /// </summary>
        /// <param name="pc">собьект страницы корзины</param>
        /// <param name="sumPrev">изначальная сумма</param>
        public static void WaitSumChange(PageCart pc, double sumPrev) 
        {
                bool check = true;
                for (int i = 0; i < Wtime * 3; i++)
                {
                try
                {
                    double sumLast = pc.NumberSum;
                
                if (sumPrev != sumLast)
                    {
                        check = false;
                        break;
                    }
                    Thread.Sleep(500);
                }
                catch (StaleElementReferenceException)
                { }
                }
                if (check) { throw new ArgumentException(String.Format("Sum not chenged during 8 sec")); }
        }

        /// <summary>
        /// Заполняет лист с нулевыми значениями элементов числами из другого листа, оставляя нулевыми элементы которым не хватило значений 
        /// </summary>
        /// <param name="listZero">нулевой лист</param>
        /// <param name="listFull">заполненый лист</param>
        /// <returns></returns>
        public static List<double> FillEmptyList(List<double> listZero, List<IWebElement> listFull) 
        {
            try
            {
                for (int i = 0; i < listZero.Count; i++)
                {
                    listZero[i] = double.Parse(listFull[i].Text.Split(" ")[1], CultureInfo.InvariantCulture);
                }
            }
            catch (Exception)
            {
                return listZero;
            }
            return listZero;
        }


    }
    
}
