﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageSearch : PagePattern
    {
        public PageSearch() { Url = "search/?q=qwerttrewq1234554321"; }
        public IWebElement ButtonBackMainPage => WaitElement(By.XPath("(//ul[@itemscope]//span[@itemprop])[1]"), Wtime);
        public IWebElement TextProductName => WaitElement(By.XPath("//div[contains(@class,'cont')]//div[contains(@id,'bx')]//div[contains(@class,'item-tit')]//a"), Wtime);
        public string TextProductNameString => GeneralFunctions.NoSpaces(TextProductName.Text);
        public IWebElement TextNoResultSearch => WaitElement(By.XPath("//div[contains(@class,'search-no-res')]"), Wtime);
       
    }
}
