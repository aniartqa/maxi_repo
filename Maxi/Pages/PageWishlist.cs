﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageWishlist: PagePattern
    {
        public PageWishlist() { Url = "personal/wishlist/"; }
        public IWebElement ButtonWishlistGroop => WaitElement(By.XPath("//div[contains(@class,'page wishlist-page')]//div[@id='selected-item']"), Wtime);
      
        public IWebElement ButtonDeleteItem => WaitElement(By.XPath("//a[contains(@href,'delete_item')]"), Wtime);
        public By TextCardProduct => By.XPath("//div[contains(@class,'one-cat-item')]");


        public IWebElement PopupProductAddedWishList => WaitElement(By.XPath("//div[@id='add-favorites-modal']"), Wtime);
        public IWebElement PopupChooseWishList => WaitElement(By.XPath("//div[@id='add-favorites-modal']//form"), Wtime);
        public IWebElement ButtonChooseWishList => WaitElement(By.XPath("(//div[@id='add-favorites-modal']//span)[1]"), Wtime);
        public IWebElement ButtonAddWishList => WaitElement(By.XPath("//div[@id='add-favorites-modal']//input[@type='submit']"), Wtime);


        public IWebElement TextWishListGroopInfo => WaitElement(By.XPath("//div[contains(@class,'sort-wish')]"), Wtime);
        public IWebElement TextProductCart => WaitElement(By.XPath("//div[contains(@class,'one-cat-item-wish')]//div[contains(@class,'one-cat-item-tit')]"), Wtime);
        public string TextProductName => GeneralFunctions.NoSpaces(WaitElement(By.XPath("//div[contains(@class,'one-cat-item-wish')]//div[contains(@class,'one-cat-item-tit')]"), Wtime).Text);


        public IWebElement ButtonSliderLiderAddWishlist => WaitElementNoFullLoad(By.XPath("//div[@class = 'carus-news']//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//a[contains(@class,'fav add')]"), Wtime);
        public string ButtonSliderLiderProductName => GeneralFunctions.NoSpaces((WaitElement(By.XPath("//div[@class = 'carus-news']//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//div[contains(@class,'news-slid-tit')]//a"), Wtime)).Text);


        public IWebElement ButtonByWishlist => WaitElementNoFullLoad(By.XPath("//a[contains(@class,'by-item')]"), Wtime);


        public IWebElement ButtonCreateWishlist => WaitElementNoFullLoad(By.XPath("//button[contains(@class,'creating-button')]"), Wtime);
        public IWebElement ButtonOpenWishlist => WaitElementNoFullLoad(By.XPath("//a[@id='open-wishlist']"), Wtime);
        public IWebElement ButtonChooseWishlist => WaitElementNoFullLoad(By.XPath("//ul[contains(@class,'my-wishlist')]//li[2]//a"), Wtime);
        public IWebElement ButtonDeleteWishlist => WaitElementNoFullLoad(By.XPath("//img[@alt='delete-button']"), Wtime);
        public IWebElement ButtonFormCreateWishlist => WaitElementNoFullLoad(By.XPath("//form[@action]//button[@type='submit' and contains(@class, 'creating-button')]"), Wtime);

        public IWebElement TextSelectedWishlist => WaitElementNoFullLoad(By.XPath("//div[@id='selected-item']"), Wtime);
        public By TextSelectedWishlistSelector => By.XPath("//div[@id='selected-item']");
        public By TextFieldListNameSelector => By.XPath("//input[@name='NAME' and @placeholder]");
        public IWebElement TextFieldListName => WaitElementNoFullLoad(By.XPath("//input[@name='NAME' and @placeholder]"), Wtime);
        public IWebElement TextFieldListNameHide => WaitElementNoFullLoad(By.XPath("//input[@name='NAME' and @placeholder]"), Wtime, new Condition(GeneralFunctions.NotDisplayed));

    }
}
