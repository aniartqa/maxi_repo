﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageProduct : PagePattern//страница товара
    {
        public IWebElement ButtonBy => WaitElement(By.XPath(
            "//form//div[contains(@class,'prod-by')]//input"), Wtime);//кнопка Купить на страице товара
        public IWebElement ButtonByBanner => WaitElement(By.XPath(
            "//div[@id='fix-info']//div[contains(@class,'prod-by')]//input"), Wtime);//кнопка Купить в плавающем баннере на странице товара
        public IWebElement ButtonCompare => WaitElement(By.XPath(
            "//div[contains(@class,'add2compare')]//div[contains(@class,'add-to-sr-text')]"), Wtime);//кнопка Добавить в сравнение на странице товара
        public IWebElement ButtonCompareActive => WaitElement(By.XPath(
            "//div[contains(@class,'js_add2compare in-comp added')]//div[contains(@class,'add-to-sr-text')]"), Wtime);//активированая кнопка Добавить в сравнение на странице товара
        public IWebElement FormComments => WaitElement(By.XPath(
            "//form[@id='REVIEWS_CAL_data']"), Wtime);//форма Оставить отзыв на странице товара
        public IWebElement ButtonAllCredits => WaitElement(By.XPath(
            "//a[contains(@href,'all-options')]"), Wtime);//кнопка Все варианты кредитов на странице товара
        public IWebElement ButtonCredit => WaitElement(By.XPath(
            "(//div[contains(@class,'cred-variants cred-table')]//div[contains(@class,'jq-radio') and @unselectable])[1]"), Wtime);//кнопка выбора кредита на странице товара
        public IWebElement ButtonCreditActive => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'focused')]"), Wtime, new Condition(GeneralFunctions.Exist));//активированая кнопка выбора кредита на странице товара
        public IWebElement ButtonByCredit => WaitElementNoFullLoad(By.XPath(
            "//input[@id='buy_on_credit']"), Wtime, new Condition(GeneralFunctions.Exist));//кнопка Оформить кредит на странице товара
        public IWebElement ButtonAllCharacters => WaitElement(By.XPath(
            "//div[contains(@class,'show-all-char')]//a"), Wtime);//кнопка Показать все характеристики на странице товара



        public string TextCharacterType => GeneralFunctions.NoSpaces(WaitElement(By.XPath(
            "//tbody//tr[3]//td[contains(@class,'param')]"), Wtime).Text);//отображение третей характеристики товара на странице товара
        public string TextCharacterTypeScreenSize => GeneralFunctions.NoSpaces(WaitElement(By.XPath(
           "//tbody//tr[4]//td[contains(@class,'param')]"), Wtime).Text);
        public string TextCharacterTypeEnergy => GeneralFunctions.NoSpaces(WaitElement(By.XPath(
           "//tbody//tr[24]//td[contains(@class,'param')]"), Wtime).Text);
        public string TextCharacterTypeCapability => GeneralFunctions.NoSpaces(WaitElement(By.XPath(
           "//tbody//tr[4]//td[contains(@class,'param')]"), Wtime).Text);

        public string TextProductCode => GeneralFunctions.NoSpaces(((WaitElement(By.XPath(
            "//div[contains(@id,'bx')]//div[contains(@class,'product-info')]//div[contains(@class,'prod-art')]"),
            Wtime)).Text).Split(":")[1]);//отображение кода товара на странице товара
        public string TextProductName => GeneralFunctions.NoSpaces((WaitElement(By.XPath(
            "//div[contains(@class,'page')]//h1"), Wtime)).Text);//отображение названия товара на странице товара


        public IWebElement ButtonAddWishList => WaitElement(By.XPath(
            "(//div[contains(@class,' fav-container')]//div[contains(@class,'add-list')]//div[contains(@class,'add-to-sr-text-in')])[1]"), Wtime);//отображение названия товара на странице товара

        
    }
}
