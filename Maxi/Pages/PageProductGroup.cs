﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageProductGroup :PagePattern//страница группы товаров
    {
        public string UrlSmartphone => baseUrlLocal + "telefonlar-ve-plansetler/telefonlar/smartfonlar/";
        public string UrlTelevisor => baseUrlLocal + "televizorlar-ve-audio-video/televizorlar/";
        public string UrlFridge => baseUrlLocal + "meiset-texnikasi/stasionar-texnikasi/soyuducular/";
        public string UrlUsb => baseUrlLocal + "komputer-texnikasi/usb-toplayici/";

        public PageProductGroup() { Url = "telefonlar-ve-plansetler/telefonlar/smartfonlar/"; }

        public string SecondPage => Drivers.baseUrlLocal+ Url + "?PAGEN_100=2";//Url следующей страницы в группе товаров
        public IWebElement ButtonBySliderSaw => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'most-view')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//a[contains(@class,'by-item')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопна купить в слайдере Просмотренные товары на странице группы товаров
        public IWebElement ButtonBy => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'cont')]//div[contains(@id,'bx')][1]//div[(contains(@class,'hide-info'))]//a[contains(@class,'by-item')][1]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Купить в карточке товара на странице группы товаров
        public IWebElement ButtonCompare => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'cont')]//div[contains(@id,'bx')][1]//div[(contains(@class,'hide-info'))]//a[contains(@class,'comp')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Добавить в сравнение в карточке товара на странице группы товаров
        public IWebElement ButtonCompareActive => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'cont')]//div[contains(@id,'bx')][1]//div[(contains(@class,'hide-info'))]//a[contains(@class,'comp added')]"), 
            Wtime*2, new Condition(GeneralFunctions.ExistDisplayed));//активированная кнопка Добавить в сравнение в карточке товара на странице группы товаров
        public IWebElement CardProductSliderSaw => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'most-view')]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//отображение карточки товара в слайдере Просмотренные товары на странице группы товаров
        public IWebElement TextCardProduct => WaitElementNoFullLoad(By.XPath(
            "//div[contains(@class,'cont')]//div[contains(@id,'bx')][1]"), Wtime, 
            new Condition(GeneralFunctions.ExistDisplayed)); //отображение карточки товара на странице группы товаров
        public IWebElement FormFilters=> WaitElement(By.XPath(
            "//div[contains(@id,'custom_filter_properties')]"), Wtime);//кнопки фильтров на странице группы товаров
              
        public IWebElement SliderSaw => WaitElement(By.XPath("//div[contains(@class,'most-view')]"), 
            Wtime);//отображение слайдера Вы просматривали на странице группы товаров

        public IWebElement ButtonNoveltyGroop => WaitElement(By.XPath("//aside//a[contains(@href,'novelty')]"),
            Wtime);//кнопка Показать еще Новинки на главной странице
        public IWebElement ButtonMaxiDiscountGroop => WaitElement(By.XPath("//aside//a[contains(@href,'maxi-discount')]"), 
            Wtime);//кнопка Показать все скидки на главной странице


        public IWebElement ButtonProductName => WaitElement(By.XPath(
            "//div[contains(@id,'bx')]//div[contains(@class,'one-cat-item-tit')]//a[@href]"), Wtime);
        //кнопка-ссылка на страницу товара на странице группы товаров
        public double TextProductPriceFirst => double.Parse(GeneralFunctions.LeftNumbers(WaitElement(By.XPath(
            "(//div[contains(@class,'cont')]//div[contains(@id,'bx')]//div[contains(@class,'price')]//span[contains(@class,'nowrap')])[1]"), 
            Wtime).Text));//отображение цены у первого товара на странице группы товаров
        public double TextProductPriceSecond => double.Parse(GeneralFunctions.LeftNumbers(WaitElement(By.XPath(
            "(//div[contains(@class,'cont')]//div[contains(@id,'bx')]//div[contains(@class,'price')]//span[contains(@class,'nowrap')])[2]"), 
            Wtime).Text));//отображение цены у второго товара на странице группы товаров
        public List<IWebElement> TextListProducts => (driver.FindElements(By.XPath(
            "(//div[contains(@id,'bx')]//div[contains(@class,'one-cat-item-tit')]//a[contains(@href,'telefonlar')])"))).ToList();//список товаров на странице группы товаров

        public string TextFilterCharacterBrand => GeneralFunctions.NoSpaces((((WaitElement(By.XPath(
            "((//div[contains(@class,'comments_container')])[3]//label//a[@onclick])[1]"), Wtime)).Text).Split("(")[0]));//название первой характеристики в фильтрах на странице группы товаров
        public string TextFilterCharacterScreen => GeneralFunctions.NoSpaces((((WaitElement(By.XPath(
            "((//div[contains(@class,'vid_comments_head')])[4]//label//a//span)[1]"), Wtime)).Text).Split("(")[0]));//название первой характеристики в фильтрах на странице группы товаров
        public string TextFilterCharacterEnergy => GeneralFunctions.NoSpaces((((WaitElement(By.XPath(
           "((//div[contains(@class,'vid_comments_head')])[6]//label//a//span)[1]"), Wtime)).Text).Split("(")[0]));//название первой характеристики в фильтрах на странице группы товаров
        public string TextFilterCharacterCapasity => GeneralFunctions.NoSpaces((((WaitElement(By.XPath(
                   "((//div[contains(@class,'vid_comments_head')])[4]//label//a//span)[1]"), Wtime)).Text).Split("(")[0]));//название первой характеристики в фильтрах на странице группы товаров


        public IWebElement ButtonFilterCharacterActive => WaitElement(By.XPath(
            "//div[contains(@id,'custom_filter_active')]//div[contains(@class,'one-act-filt')]//a"), Wtime);//активированная кнопка характеристики в фильтрах на странице группы товаров
        
        public IWebElement ButtonFilterCharacterBrand => WaitElement(By.XPath(
            "(//div[@id and @data-code]//div)[3]"), Wtime);//кнопка характеристики в фильтрах на странице группы товаров


         public IWebElement ButtonFilterCharacterScreen => WaitElement(By.XPath(
             "((//div[contains(@class,'vid_comments_head')])[5]//div[@id and @data-code]//div)[1]"), 
             Wtime);//кнопка характеристики в фильтрах на странице группы товаров
         public IWebElement ButtonFilterCharacterEnergy => WaitElement(By.XPath(
            "((//div[contains(@class,'vid_comments_head')])[6]//div[@id and @data-code]//div)[1]"), Wtime);//кнопка характеристики в фильтрах на странице группы товаров
        public IWebElement ButtonFilterCharacterCapasity => WaitElement(By.XPath(
            "((//div[contains(@class,'vid_comments_head')])[5]//div[@id and @data-code]//div)[1]"), Wtime);//кнопка характеристики в фильтрах на странице группы товаров



        public IWebElement ButtonPriceSort => WaitElement(By.XPath("//a[contains(@data-jsl,'CATALOG_PRICE')]"), Wtime);//кнопка сортировки по цене на странице группы товаров
        public IWebElement ButtonPriceSortActive => WaitElement(By.XPath("" +
            "//a[contains(@data-jsl,'CATALOG_PRICE') and contains(@class,'act')]"), Wtime);//активированная кнопка сортировки по цене на странице группы товаров

        public IWebElement ButtonRollFilterActive => WaitElement(By.XPath("//div[contains(@class,'one-act-filt')]//a"), Wtime);//активированная кнопка ползунка выбора цены на странице группы товаров
        public By ButtonRollFilterActiveSelctor => By.XPath("//div[contains(@class,'one-act-filt')]//a");//css селектор кнопки ползунка выбора цены
        public IWebElement TextBoxPriceRollFirst => WaitElementNoFullLoad(By.XPath("//input[@id='filter_price_from']"), Wtime);//поле ввода минимальной цены на странице группы товаров
        public IWebElement TextBoxPriceRollSecond => WaitElementNoFullLoad(By.XPath("//input[@id='filter_price_to']"), Wtime);//поле ввода максимальной цены на странице группы товаров


        public IWebElement ButtonPadingTwo => WaitElement(By.XPath("(//div[contains(@class,'pagi')]//a[@href])[1]"), Wtime);//кнопка переключения на страницу номер 2 на странице группы товаров
        public IWebElement ButtonPadingTwoActive => WaitElement(By.XPath("" +
            "(//div[contains(@class,'pagi')]//li[2]//a[contains(@class,'act')])"), Wtime);//актовированная кнопка переключения на страницу номер 2 на странице группы товаров
        public IWebElement ButtonShowMore => WaitElement(By.XPath("//a[contains(@onclick,'getMoreElements')]"), Wtime);//кнопка Показать еще 24 товара на странице группы товаров


        public IWebElement ButtonChangeShow => WaitElement(By.XPath("//a[contains(@data-jsl,'type=list')]//span"), Wtime);//кнопка изменения отображения списка товаров на странице группы товаров
        public IWebElement TextChangeShow => WaitElement(By.XPath("//div[contains(@class,'item-fuct')]"), Wtime);//отображение карточки товара изменненной раскладки списка товаров 

        public string TextProductName => GeneralFunctions.NoSpaces(WaitElement(By.XPath("(//div[contains(@id,'bx')]//div[contains(@class,'one-cat-item-tit')])[1]"), Wtime).Text);
        public IWebElement ButtonAddWishList => WaitElement(By.XPath("(//div[contains(@id,'bx')]//a[contains(@class,'fav add')])[1]"), Wtime);

        public string TextProductNameSlider => GeneralFunctions.NoSpaces(WaitElement(By.XPath("//div[contains(@class,'most-view')][2]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//div[contains(@class,'news-slid-tit')]//a"), Wtime).Text);
        public IWebElement ButtonAddWishListSlider => WaitElement(By.XPath("//div[contains(@class,'most-view')][2]//div[contains(@class,'one-news-slid') and @aria-hidden='false'][1]//div[contains(@class,'js_favorites')]//a[contains(@class,'add')]"), Wtime);

        public IWebElement ButtonAddWishListProductPage => WaitElement(By.XPath("//div[contains(@class,'about-prod')]//div[contains(@class,'js_favorites') and @data-product-id]"), Wtime);
        

    }
}
