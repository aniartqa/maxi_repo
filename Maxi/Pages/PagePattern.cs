﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    abstract public class PagePattern : PageBase//шаблонная страница, общий функционал для всех страниц 
    {
        
        public IWebElement ButtonUserEntrance => WaitElement(By.XPath("//a[@id='login-form']"), Wtime, 
                new Condition(GeneralFunctions.ExistDisplayedEnabled)); //кнопка ввойти в кабинет
        public IWebElement BasketProd => WaitElement(By.XPath("//div[@id='basked-prod']"), Wtime);//отображение сообщения о добавлении в корзину
        public IWebElement TextUserName => WaitElement(TextUserNameSelector, Wtime,
                new Condition(GeneralFunctions.ExistDisplayedEnabled)); //отображение имени пользователя
        public By TextUserNameSelector => By.XPath("//div[contains(@class,'lk-menu')]//a[contains(@href,'personal')]");//css селектор имени пользователя

        public IWebElement TextBoxSearch => WaitElement(By.XPath("//input[@id='searchInput']"), Wtime);//поле ввода поиска
        public IWebElement ButtonSearch => WaitElement(By.XPath("//form[@id='headerSearch']//input[@type='submit']"), Wtime);//кнопка поиска

        public IWebElement ButtonGroupPhone => WaitElement(By.XPath("//nav//a[contains(@src,'thumb-1') and @data-url]"), Wtime);//кнопка добавить
        public IWebElement ButtonSubGroupSmartphone => WaitElement(By.XPath("//nav//a[contains(@href,'telefonlar/smartfonlar')]"), Wtime);
        public IWebElement PopupMenuActive => WaitElement(By.XPath("//li[contains(@class,'menu-ico-1 act')]"), Wtime);

        public IWebElement ButtonWishlist => WaitElement(By.XPath("//a[contains(@href,'personal/wishlist')]//div"), Wtime);

        public IWebElement ButtonLocalization => WaitElement(By.XPath("(//input[@value='Lang']/following::span[contains(@class,'lang-left')])[1]"), Wtime);


        public IWebElement ButtonCheckoutForm =>
            WaitElementNoFullLoad(By.XPath("//div[contains(@class,'modal-bascet')]//a[contains(@href,'order')]"), Wtime);
        public IWebElement ButtonCheckout =>
            WaitElementNoFullLoad(By.XPath("//div[@id='basket-mini']//a[contains(@class,'order-bt')]"), Wtime);
        public IWebElement ButtonCart =>
            WaitElement(By.XPath("//div[@id='basket-mini']"), Wtime);

        public PagePattern() { Url = ""; }


    }
}
