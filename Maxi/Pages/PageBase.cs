﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TestFramework.General;
using TestFramework.Tests;

namespace TestFramework.Pages
{
    abstract public class PageBase
    {
        protected static IWebDriver driver => Drivers.dr; // 
        protected string baseUrl => Drivers.baseUrl;
        public static string baseUrlLocal => Drivers.baseUrlLocal;
        protected static int Wtime => Drivers.Wt;

        public string Url;

        protected static int timeLoadPage = Wtime*3;
        public PageBase() { }


        /// <summary>
        /// Ищет на полностью загруженной странице веб элемент по сss селектору
        /// </summary>
        /// <param name="selector">css селектор</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <returns></returns>
        public static IWebElement WaitElement(By selector, int time)
        {
            WaitPageFullLoaded();
            return GeneralFunctions.WaitElement(driver, selector, time, new Condition(GeneralFunctions.ExistDisplayedEnabled));
        }

        /// <summary>
        ///  Ищет на полностью загруженной странице веб элемент по сss селектору
        /// </summary>
        /// <param name="selector">css селектор</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <param name="cnd">обьект делегата Condition (условие для веб элемента)</param>
        /// <returns></returns>
        public static IWebElement WaitElement(By selector, int time, Condition cnd)
        {
            WaitPageFullLoaded();
            return GeneralFunctions.WaitElement(driver, selector, time, cnd);
        }

        /// <summary>
        /// Ищет на странице веб элемент по сss селектору
        /// </summary>
        /// <param name="selector">css селектор</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <returns></returns>
        public static IWebElement WaitElementNoFullLoad(By selector, int time)
        {
            return GeneralFunctions.WaitElement(driver, selector, time, new Condition(GeneralFunctions.ExistDisplayedEnabled));
        }

        /// <summary>
        /// Ищет на странице веб элемент по сss селектору
        /// </summary>
        /// <param name="selector">css селектор</param>
        /// <param name="time">максимальное время ожидания</param>
        /// <param name="cnd">обьект делегата Condition (условие для веб элемента)</param>
        /// <returns></returns>
        public static IWebElement WaitElementNoFullLoad(By selector, int time, Condition cnd)
        {
            return GeneralFunctions.WaitElement(driver, selector, time, cnd);
        }

       
        /// <summary>
        /// Переходит на страницу указаную в поле URL класса страницы
        /// </summary>
        public void Navigate()
        { 
            driver.Navigate().GoToUrl(Drivers.baseUrlLocal + Url);
            WaitPageFullLoaded();
        }

        /// <summary>
        /// Ждет пока страница полностью загрузиться
        /// </summary>
        /// <param name="time">максимальное время ожидания</param>
        public static void WaitPageFullLoaded()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeLoadPage));

            wait.Until((x) =>
            {
                return ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete");
            });
        }
        

      



    }
}
