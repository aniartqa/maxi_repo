﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageCompare : PagePattern
    {
        public PageCompare() { Url = "compare/"; }

        public IWebElement ButtonBy => WaitElementNoFullLoad(By.XPath("//div[contains(@id,'mCSB')]//a[@data-id]"), Wtime, 
            new Condition(GeneralFunctions.ExistDisplayed));//кнопка Купить в сравнении
        public IWebElement ButtonByBanner => WaitElementNoFullLoad(By.XPath("//div[contains(@class,'compare-wrap-page')]//a[@data-id]"), Wtime, 
            new Condition(GeneralFunctions.ExistDisplayed));//кнопка купить в плавающем баннере сравнения
        public IWebElement ButtonMainPage => WaitElement(By.XPath("//a[contains(@itemprop,'item')]"), 
            Wtime);//кнопка возврата на главную страницу на странице сравнения
        public IWebElement ButtonAddProduct => WaitElement(By.XPath("//div[contains(@class,'compare-left')]//thead//a[contains(@class,'add-item')]"),
            Wtime);//кнопка Добавить продукт в сравнение на странице сравнения
        public IWebElement ButtonDeleteProduct => WaitElement(By.XPath("//div[contains(@id,'mCSB')]//a[contains(@class,'remove-comp')]//span"), 
            Wtime);//кнопка Удалить продукт из сравнения на странице сравнения
        public List<IWebElement> ButtonsGroops => driver.FindElements(
            By.XPath("//a[contains(@class,'change-compare-section')]")).ToList();//кнопка переключения между группами сравнений
        public IWebElement ButtonGroop => WaitElement(By.XPath("//a[contains(@class,'change-compare-section')]"), 
            Wtime);//меню с кнопками на странице сравнения
        public IWebElement ButtonDiferentCharacters => WaitElement(By.XPath("//div[contains(@id,'products_compare_mode-styler')]/div"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Показать только различия на странице сравнения
        public IWebElement ButtonPrint => WaitElement(By.XPath("//a[contains(@id,'compare_print')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//кнопка Распечатать сравнение


        public IWebElement TextCardProduct => WaitElement(By.XPath("//div[contains(@id,'mCSB')]//div[contains(@class,'compare-item-tit')]"), 
            Wtime, new Condition(GeneralFunctions.ExistDisplayed));//отображение карточки товара в сравнении
        public List<IWebElement> TextProductsNames => GeneralFunctions.CheckElementsListDisplayed(driver.FindElements(By.XPath(
            "//div[contains(@id,'mCSB')]//div[contains(@class,'compare-item-tit')]//a")).ToList());//список названий товаров в сравнении

        public string TextProductNamePrint => GeneralFunctions.NoSpaces((WaitElement(By.XPath("//div[contains(@class,'compare-item-tit')]//a"), Wtime, 
            new Condition(GeneralFunctions.ExistDisplayed))).Text);//строка с иназванием первого товара в сравнении
        public string TextFirstProductName => GeneralFunctions.NoSpaces(TextProductsNames.ElementAt(0).Text);//Строка с названием первого товара в сравнении без пробелов
        public By TextFirstProductNameSelector => By.XPath("" +
            "(//div[contains(@id,'mCSB')]//div[contains(@class,'compare-item-tit')]//a)[1]");//css селектор названия первого товара в сравнении
        public IWebElement TextSecondProductName => TextProductsNames.ElementAt(1);//ображение имени второготовара в сравнении
        public IWebElement TextEmptyProductList => WaitElement(By.XPath("" +
            "//div[contains(@class,'cmp-empty-tit')]"), Wtime);//текст Нет товаров в сравнении
        public string TextCharacterType => GeneralFunctions.NoSpaces((WaitElement(By.XPath("" +
            "//div[contains(@id,'mCSB')]//tr[contains(@class,'products-compare-attr')][3]//td/span"), Wtime)).Text);//строка значения характеристики товара в сравнении
        public List<IWebElement> TextCharacters => GeneralFunctions.CheckElementsListDisplayed(driver.FindElements(By.XPath("" +
            "//div[contains(@id,'mCSB')]//tr[contains(@class,'products-compare-attr')]//td/span")).ToList());//отображение значения характеристики товара в сравнении
        public By TextCharactersSelector => By.XPath("" +
            "//div[contains(@id,'mCSB')]//tr[contains(@class,'products-compare-attr')]//td/span");//css селектор значения характеристики товара в сравнении
        public List<IWebElement> TextProductsPrices => GeneralFunctions.CheckElementsListDisplayed(driver.FindElements(By.XPath("" +
            "//div[contains(@id,'mCSB')]//div[contains(@class,'news-slid-price ')]//span[contains(@class,'nowrap')]")).ToList());//список цен товаров на странице сравнения
        public double TextSecondProductPrice => double.Parse(GeneralFunctions.LeftNumbers((TextProductsPrices[1].Text)), 
            CultureInfo.InvariantCulture);// отображение цены второго товара в сравнении
        public static List<IWebElement> ListCharacters; //список характеристик товара
        public double TextFirstProductPrice => double.Parse(GeneralFunctions.LeftNumbers(TextProductsPrices[0].Text),
            System.Globalization.CultureInfo.InvariantCulture);//Отображение цены первого товара в сравнении
        public IWebElement Footer => WaitElementNoFullLoad(By.XPath("//div[contains(@class,'footer-top')]"), Wtime, 
            new Condition(GeneralFunctions.ExistDisplayed));// отображение футера страниці сравнения
        public IWebElement TextPrint => WaitElement(By.XPath("//h4"), Wtime, new Condition(GeneralFunctions.Exist));//отображение распечатки сравнения товаров


    }
}
