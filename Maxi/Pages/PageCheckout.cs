﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageCheckout : PagePattern
    {
        public IWebElement ButtonMenuSteps =>
            WaitElement(By.XPath("//div[@id='order_steps_menu']"), Wtime*3);
        public List <IWebElement> Products =>
            (driver.FindElements(By.XPath("//tr[@data-product_id]"))).ToList();
        public string TextProductName =>
            GeneralFunctions.NoSpaces((WaitElement(By.XPath("//tr[contains(@id, 'order_basket_item')]//a[contains(@class,'item-name')]"), Wtime)).Text);
       
        public IWebElement ButtonNextStep1 =>
            WaitElement(By.XPath("//input[@id='order_user_prop_register']"), Wtime);
        public IWebElement ButtonAdditionalOrder =>
            WaitElement(By.XPath("//div[@id='order_delivery_comment']//span"), Wtime);
        public IWebElement TextFieldAdditionalOrder =>
            WaitElement(By.XPath("//textarea[@name='ORDER_DESCRIPTION']"), Wtime);
        public IWebElement ButtonNextStep2Step3 =>
           WaitElement(By.XPath("//input[@id='order_next_step']"), Wtime);
        public IWebElement ButtonPayment =>
           WaitElement(By.XPath("//div[@id='order_pay_system_1-styler']"), Wtime);
        public IWebElement TextBonus =>
           WaitElement(By.XPath("//div[@id='order_bonus']"), Wtime);
        public IWebElement ButtonConfirmOrder =>
           WaitElement(By.XPath("//input[@id='order_confirm']"), Wtime);

        public IWebElement ButtonPersonalCab =>
           WaitElement(By.XPath("//input[contains(@onclick,'personal')]"), Wtime*2);
        

        public IWebElement ButtonEntranceAccount =>
           WaitElement(By.XPath("(//ul[contains(@class,'tabs__caption')]//span)[2]"), Wtime*2);
        public IWebElement TextBoxEmail =>
           WaitElement(By.XPath("//input[@name='USER_LOGIN']"), Wtime);
        public IWebElement TextBoxPass =>
           WaitElement(By.XPath("//input[@name='USER_PASSWORD']"), Wtime);
        public IWebElement ButtonEntrance =>
           WaitElement(By.XPath("//input[@id='order_auth_submit']"), Wtime);
        public IWebElement TextBoxUserPhone =>
           WaitElement(By.XPath("//input[@id='order_user_prop_phone']"), Wtime);


        public double TextSum => double.Parse(GeneralFunctions.LeftNumbers(WaitElement(
            By.XPath("(//tr[contains(@class,'tab-order-res')]//div[contains(@class,'res-price res-style-1')]//b)[1]"), Wtime).Text));

        public IWebElement CheckBoxPayCard =>
               WaitElement(By.XPath("//div[@id='order_pay_system_7-styler']"), Wtime);
        public IWebElement ButtonPayCard =>
           WaitElement(By.XPath("//a[contains(@href,'order/payment/') and @target]"), Wtime*2);

    }
}
