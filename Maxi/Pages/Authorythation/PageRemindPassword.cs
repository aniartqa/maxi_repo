﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages.Authorythation
{
    public class PageRemindPassword : PagePattern
    {

        public IWebElement TextBoxEmail =>
            WaitElement(By.XPath("//input[@name='form-recovery__email']"), Wtime);
        public PageRemindPassword() { Url = ""; }
    }
}
