﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using TestFramework.General;

namespace TestFramework.Pages
{
    public class PageAuthotyzation: PagePattern
    {
        public IWebElement ButtonRemindPassword => 
            WaitElement(By.XPath("//div[contains(@class,'forgot-pass')]//a[@href = '#ForgPass']"), Wtime);


        public IWebElement ButtonRegistration =>
           WaitElement(By.XPath("//a[@id = 'reg-form']"), Wtime);


        public IWebElement ButtonEntrance => 
            WaitElement(By.XPath("//form[@id='form-identification-form']//input[@class='bt-log']"), Wtime);
        public IWebElement ButtonСancel => 
            WaitElement(By.XPath("//input[@onclick = '$.fancybox.close()']"), Wtime);


        public IWebElement ButtonFacebookEntrance => 
            WaitElement(By.XPath("//div[contains(@class,'fancybox-outer')]//a[contains(@onclick,'facebook.com')]"), Wtime);
        public IWebElement ButtonGoogleEntrance => 
            WaitElement(By.XPath("//div[contains(@class,'fancybox-outer')]//a[contains(@onclick,'accounts.google.com')]"), Wtime);
        
        public IWebElement ButtonPersonalCabEntrance =>
            WaitElement(By.XPath("//input[@name='Login']"), Wtime);

        public IWebElement PopupAuth =>
    WaitElement(By.XPath("//div[contains(@class,'fancybox-wrap fancybox-desktop')]"), Wtime);

        public By selectorPopupAuth =>
           By.XPath("//div[contains(@class,'fancybox-wrap fancybox-desktop')]");

        public IWebElement TextBoxEmail =>
          WaitElement(By.XPath("//input[@name = 'USER_LOGIN']"), Wtime);
        public IWebElement TextBoxPassword =>
            WaitElement(By.XPath("//input[@name = 'USER_PASSWORD']"), Wtime);

        public IWebElement TextError =>
            WaitElement(By.XPath("//div[contains(@class,'modal-form-inp error')]"), Wtime);



        // driver.switchTo().activeElement()

    }
}
