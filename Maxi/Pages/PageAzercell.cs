﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestFramework.Pages
{
    public class PageAzercell: PagePattern // Страница группы товаров "Номера телефона"
    {
        public PageAzercell() { Url = "azercell-nomreler/"; }

        
        public IWebElement ButtonBuyCartProduct => WaitElement(By.XPath(
            "(//div[contains(@class,'cont-cat')]//div[contains(@class,'numb-bt-in')])[1]"), Wtime); // кнопка Купить в карточке товара на странице группы товаров "Номера телефона"
        public IWebElement ButtonBy => WaitElement(By.XPath(
            "//input[contains(@class,'js_add2basket') and contains(@data-id,'')]"), Wtime); // кнопка Купить на странице товара-номера телефона
        public IWebElement TextCountAzercell => WaitElement(By.XPath(
            "(//div[contains(@class,'pagi')]//li//a)[5]"), Wtime);// последняя страница пагинации на странице группы товаров "Номера телефона"
        public IWebElement ButtonSecondPageAzercell => WaitElement(By.XPath(
            "(//div[contains(@class,'pagi')]//li//a)[2]"), Wtime);// вторая страница пагинаци на странице группы товаров "Номера телефона"
        public IWebElement TextProductName => WaitElement(By.XPath(
            "(//div[contains(@class,'numb-tit')]/a[contains(@href,'azercell-nomreler')])[1]"), Wtime); // название товара в карточке товара на странице группы товаров "Номера телефона"

    }
}
